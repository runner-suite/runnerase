# runnerase

[![pipeline status](https://gitlab.com/runner-suite/runnerase/badges/main/pipeline.svg)](https://gitlab.com/runner-suite/runnerase/-/commits/main)
[![Documentation](https://img.shields.io/badge/Documentation-latest-blue.svg)](https://runner-suite.gitlab.io/runnerase/latest/)

> An Interface between the Runner Neural Network Energy Representation
  (RuNNer) and the Atomic Simulation Environment (ASE).

## Key Features

- **readers and writers** for all RuNNer file formats.
- `Runner`, an **ASE calculator** class for managing the RuNNer workflow.
- `SymmetryFunction`/`SymmetryFunctionSet`, classes for generating and
  manipulating **atom-centered symmetry functions**.
- A `plot`ting library for generating **nice figures** from RuNNer data.
- **storage classes** for atom-centered symmetry function values, weights
  and scaling data of the atomic neural networks, the split between
  training and testing set, and the results of a RuNNer fit.
- Customizable automatic training of a full potential from a dataset.
- Active learning module.
- Connection to LAMMPS for large-scale simulations.
- Ability to submit and retrieve jobs to the SLURM queueing system.

## Installation

The package can be easily installed via pip. If they are not available, the
dependencies `ASE`, `numpy` and scipy will be automatically installed.
The use of the optional dependency `tqdm` (progressbar) is highly
recommended for following the calculation progress. 

```sh
$ pip install runnerase[progressbar]
```

[**Please check out our documentation for further information!**](https://runner-suite.gitlab.io/runnerase/latest/)

## Questions, Contributions, Suggestions

Everyone is warmly invited to open issues and merge requests here on Gitlab
or [contact the developer](mailto:alexander.knoll@rub.de).

## Credits

This software is an extension of the [Atomic Simulation Environment (ASE)](https://wiki.fysik.dtu.dk/ase/).

The code is written and maintained by Alexander Knoll - @aknoll - [alexknoll@mailbox.org](mailto:alexander.knoll@rub.de).

## License

This software is distributed under the GPLv3. For details, see [LICENSE](LICENSE).

