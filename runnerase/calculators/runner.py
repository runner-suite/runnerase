"""Implementation of an ASE calculator for machine-learning with RuNNer.

This modul contains an implementation of a `Runner` calculator for
training and evaluating HDNNPs with RuNNer.
"""

from typing import Union, Optional, List, Callable, Dict, Any

import os
import threading

import numpy as np

from ase.atoms import Atoms
from ase.calculators.calculator import (FileIOCalculator, compare_atoms,
                                        CalculatorSetupError, Parameters)

from runnerase import io
from runnerase.storageclasses import (RunnerSymmetryFunctionValues,
                                      RunnerSplitTrainTest, RunnerWeights,
                                      RunnerScaling)
from runnerase.symmetry_functions.wrappers import SymmetryFunctionSet
from runnerase.utils.defaultoptions import DEFAULT_PARAMETERS
from runnerase.utils.atoms import get_elements
from runnerase.io.restart import read_runnerase
from runnerase.io.results import (read_results_mode1,
                                  read_results_mode2,
                                  read_results_mode3,
                                  check_successful)

from runnerase.plot import RunnerPlots
from runnerase.analysis import RunnerAnalysis

try:
    from tqdm import tqdm
    ENABLE_TQDM = True
except ImportError:
    ENABLE_TQDM = False


# RuNNer class inherits from FileIOCalculator and therefore requires a lot
# of instance attributes and ancestors.
# pylint: disable=[too-many-ancestors, too-many-instance-attributes]
class Runner(FileIOCalculator):
    """Class for training and evaluating neural network potentials with RuNNer.

    The default parameters are mostly those of the RuNNer Fortran code.
    There are, however, a few exceptions. This is supposed to facilitate
    typical application cases when using RuNNer via ASE. These changes are
    documented in `DEFAULT_PARAMETERS` (in `ase.io.runner.defaultoptions`).

    RuNNer operates in three different modes:

    - **Mode 1**: Calculation of symmetry function values. Symmetry functions
                  are many-body descriptors for the chemical environment of an
                  atom.
    - **Mode 2**: Fitting of the potential energy surface.
    - **Mode 3**: Prediction. Use the previously generated high-dimensional
                  potential energy surface to predict the energy and force
                  of unknown chemical configurations.

    The different modes generate a lot of output:

    - **Mode 1**:
        - `sfvalues`:       The values of the symmetry functions for each
                            atom.
        - `splittraintest`: which structures belong to the training and which
                            to the testing set. ASE needs this
                            information to generate the relevant input files
                            for RuNNer Mode 2.
    - **Mode 2**:
        - `fitresults`:     The performance of the training process.
        - `weights`:        The neural network weights.
        - `scaling`:        The symmetry function scaling factors.

    - **Mode 3**:
        - `energy`
        - `forces`

    Examples
    --------
    Run Mode 1 from scratch with existing input.nn and input.data files.

    >>> dataset = read('input.data', ':', format='runnerdata')
    >>> options = read_runnerconfig('input.nn')
    >>> RO = Runner(dataset=dataset, **options)
    >>> RO.run(mode=1)

    Restart Mode 1:

    >>> RO = Runner(restart='mode1/mode1')
    >>> print(RO.results['sfvalues'])

    Run Mode 2:

    >>> RO = Runner(restart='mode1/mode1')
    >>> RO.run(mode=2)
    >>> print(RO.results['fitresults'])

    Update some input parameters:

    >>> RO.set(epochs=20)

    Restart Mode 2 and run Mode 3:

    >>> RO = Runner(restart='mode2/mode2')
    >>> RO.run(mode=3)

    Run Mode 3 with self-defined weights and scaling data:

    >>> RO = Runner(
    >>>    scaling=scaling,
    >>>    weights=weights,
    >>>    dataset=dataset,
    >>>    **options
    >>> )
    >>> RO.atoms = Bulk('C')
    >>> RO.get_potential_energy()

    """

    implemented_properties = ['energy', 'forces', 'stress', 'charges',
                              'sfvalues', 'splittraintest',
                              'fitresults', 'weights', 'scaling']
    command = 'RuNNer.x > PREFIX.out'

    discard_results_on_any_change = False

    default_parameters = DEFAULT_PARAMETERS

    # Explicit is better than implicit. By passing all arguments explicitely,
    # the argument types can be narrowly specified.
    # pylint: disable=too-many-arguments
    def __init__(
        self,
        restart: Optional[str] = None,
        ignore_bad_restart_file: object = FileIOCalculator._deprecated,
        perform_check_successful: bool = True,
        label: Optional[str] = None,
        directory: Optional[str] = '.',
        atoms: Optional[Atoms] = None,
        dataset: Optional[List[Atoms]] = None,
        weights: Optional[RunnerWeights] = None,
        scaling: Optional[RunnerScaling] = None,
        sfvalues: Optional[RunnerSymmetryFunctionValues] = None,
        splittraintest: Optional[RunnerSplitTrainTest] = None,
        **kwargs
    ) -> None:
        """Construct RuNNer-calculator object.

        Parameters
        ----------
        restart:
            Directory and label of an existing calculation for restarting.
        ignore_bad_restart_file:
            Deprecated. See ASE documentation for details.
        perform_check_successful:
            Whether to raise an error if a calculation was unsuccessful.
        label:
            Prefix to use for filenames (label.in, label.txt, ...).
            Default is 'runner'.
        directory:
            Path to the calculation directory.
        atoms:
            Atoms object to be attached to this calculator.
        dataset:
            List of ASE Atoms objects used for training the neural network
            potential. Mandatory for RuNNer Mode 2.
        weights:
            Weights and bias values of atomic neural networks.
        scaling:
            Symmetry function scaling data.
        sfvalues:
            Symmetry function values.
        splittraintest:
            The assignment of structures in `dataset` to a train and test set.
        **kwargs:
            Arbitrary key-value pairs can be passed to this class upon
            initialization. They are passed on to the base class. Useful for
            passing RuNNer options to the `parameters` dictionary.
        """
        # Store optional input parameters as class properties.
        self.dataset = dataset
        self.weights = weights
        self.scaling = scaling
        self.sfvalues = sfvalues
        self.splittraintest = splittraintest
        self.parameters: Parameters = Parameters()

        # For progress tracking.
        self.perform_check_successful = perform_check_successful
        self.start_check_progress = False
        self.progress_bar: tqdm = None
        self.show_progressbar: bool = True
        self.progressbar_freq: float = 1e-3
        self.progressbar_position: int = 0
        self.terminated = False

        # Initialize the base class.
        FileIOCalculator.__init__(
            self,
            restart=restart,
            ignore_bad_restart_file=ignore_bad_restart_file,
            label=label,
            atoms=atoms,
            directory=directory,
            **kwargs)

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        trained = False
        if 'energy' in self.results:
            trained = True

        n_structures = 0
        if self.dataset is not None:
            n_structures = len(self.dataset)

        return (f'{self.__class__.__name__}'
                f'(n_structures={n_structures}, '
                f'trained={trained})')

    @property
    def elements(self) -> List[str]:
        """Show the elements pertaining to this calculator.

        Store chemical symbols of elements in the parameters dictionary.

        This routine returns (in decreasing order of relevance):
            1. the elements given in `self.parameters['elements']`,
            2. all elements in `self.dataset`,
            3. all elements in `self.atoms`

        If `self.parameters['elements']` was previously not set, it will be
        `set` with the information extracted from `self.dataset` or
        `self.atoms`.
        This means that the calculator will automatically grep elements from
        the attached dataset upon starting a calculation, because it will
        eventually try to access `self.elements`.

        Raises
        ------
        CalculatorSetupError
            Raised if neither a `dataset` nor an `Atoms` object have been
            defined and `self.parameters['elements']` is empty.

        Returns
        -------
        elements:
            Elements can either be set directly from a list of strings, or
            they are automatically extracted from the attached dataset or Atoms
            objects on the calculator.

        """
        elements = self.parameters['elements']

        if elements is None:
            self.set_elements()
            elements = self.parameters['elements']

        return elements

    @elements.setter
    def elements(self, elements: List[str]) -> None:
        """Store chemical symbols of elements in the parameters dictionary.

        This routine adjusts the RuNNer keywords 'elements'
        and 'number_of_elements' based on the provided `elements`.

        Parameters
        ----------
        elements:
            The list of elements which will be stored.

        """
        self.parameters['elements'] = elements
        self.parameters['number_of_elements'] = len(elements)

    def set_elements(self, elements: Optional[List[str]] = None) -> None:
        """Set the `elements` attribute based on `dataset` or `atoms`.

        This routine automatically determined the elements of this calculator
        based on either the attached dataset or the attached Atoms object.
        One may optionally supply a list of elements that will be set.

        Parameters
        ----------
        elements:
            The list of elements which will be stored.

        """
        # If no elements were specified, set them automatically based on the
        # attached dataset or Atoms object.
        if elements is None:
            images = self.dataset or [self.atoms]  # type: ignore

            # Elements will either be extracted from the training dataset or
            # from the `Atoms` object to which the calculator has been attached.
            if images is None:
                raise CalculatorSetupError('Please specify a custom list of '
                      + 'elements, or attach a dataset or Atoms object to the '
                      + 'calculator.')

            elements = get_elements(images)

        self.elements = elements

    @property
    def symmetryfunctions(self) -> SymmetryFunctionSet:
        """Show the specified short-range symmetry function parameters.

        Returns
        -------
        symfunction_short:
            The collection of symmetry functions stored under the RuNNer
            parameter 'symfunction_short'.

        """
        return self.parameters['symfunction_short']

    @symmetryfunctions.setter
    def symmetryfunctions(
        self,
        sfset: SymmetryFunctionSet
    ) -> None:
        """Add symmetry functions to the currently stored ones.

        This routine add either a single symmetry function or a whole set of
        symmetry functions to the storage container under
        `self.parameters['symfunction_short'].`

        """
        self.parameters['symfunction_short'] = sfset

    @property
    def plot(self) -> RunnerPlots:
        """Create a plotting interface."""
        if self.dataset is None:
            raise RuntimeError('Please attach a dataset to the calculator '
                               + 'to generate plots.')
        return RunnerPlots(self.dataset, self.results)

    @property
    def analyze(self) -> RunnerAnalysis:
        """Create an analysis interface."""
        if self.dataset is None:
            raise RuntimeError('Please attach a dataset to the calculator '
                               + 'to generate plots.')
        return RunnerAnalysis(self.dataset, self.results)

    # pylint: disable=too-many-branches
    # Reason: As this is a central function to run a Runner calculation,
    # it needs to make sure that all parameters are there to be successful.
    # This requires many if-checks (one more than allowed by default).
    def run(
        self,
        mode: Optional[int] = None
    ) -> None:
        """Execute RuNNer Mode 1, 2, or 3.

        Parameters
        ----------
        mode:
            The RuNNer mode that will be executed. If not given,
            `self.parameters.runner_mode` will be evaluated.

        Raises
        ------
        CalculatorSetupError:
            Raised if neither `self.dataset` nor `self.atoms` have been defined.
            This would mean that no structure data is available at all.

        """
        # If not given, use the mode stored in the `parameters`. Otherwise,
        # update parameters with the requested `mode`.
        if mode is None:
            mode = self.parameters['runner_mode']
        else:
            self.set(runner_mode=mode)

        # Set the correct calculation label.
        if self.prefix is None:
            self.prefix = f'mode{mode}'

        if self.label is None:
            self.label = f'{self.directory}/mode{mode}/mode{mode}'

        # Set the correct elements of the system.
        self.set_elements()

        # RuNNer can either be called for a single ASE Atoms object to
        # which the calculator has been attached (`self.atoms`) or for the
        # whole dataset (`self.dataset`).
        # `dataset` takes precedence over the attached `atoms` object.
        atoms = self.dataset or self.atoms

        # If neither `self.dataset` nor `self.atoms` has been defined yet,
        # raise an error.
        if atoms is None:
            raise CalculatorSetupError('Please attach a training dataset '
                                       'or an Atoms object to this calculator.')

        # If no seed was specified yet, choose a random value.
        if 'random_seed' not in self.parameters:
            self.set(random_seed=np.random.randint(1, 1000))

        # Start the calculation by calling the `get_property` method of the
        # parent class. Each mode will return more than one possible result,
        # but specifying one is sufficient at this point.
        properties = ['sfvalues', 'fitresults', 'energy']

        if self.parameters['runner_mode'] == 1:
            total = 1 if self.dataset is None else len(self.dataset)
            desc = 'Mode 1: No. Structures'
        elif self.parameters['runner_mode'] == 2:
            total = self.parameters['epochs']
            desc = 'Mode 2: No. Epochs'
        elif self.parameters['runner_mode'] == 3:
            total = 1 if self.dataset is None else len(self.dataset)
            desc = 'Mode 3: No. Structures'
        else:
            total = 0

        if ENABLE_TQDM:
            with tqdm(
                total=total,
                disable=not self.show_progressbar,
                position=self.progressbar_position,
                desc=desc
            ) as pbar:
                if self.show_progressbar:
                    self.start_check_progress = True
                    self.progress_bar = pbar

                self.get_property(properties[mode - 1], atoms=atoms)
        else:
            self.get_property(properties[mode - 1], atoms=atoms)

    def read(self, label: Optional[str] = None) -> None:
        """Read atoms, parameters and calculated properties from output file(s).

        Parameters
        ----------
        label:
            The label of the calculation whose output will be parsed.

        """
        if label is None:
            label = self.label

        if label is None:
            raise CalculatorSetupError('Please provide a valid label.')

        # Call the method of the parent class, which will handle the correct
        # treatment of the `label`.
        FileIOCalculator.read(self, label)

        # Read in the dataset, the parameters and the results.
        structures, self.parameters = read_runnerase(label)

        if isinstance(structures, list):
            self.dataset = structures
        else:
            self.atoms = structures

        self.read_results()

    def read_results(self) -> None:
        """Read calculation results and store them on the calculator."""
        # Call an IO function to read all results expected in this mode.
        # results will be a dictionary.
        mode = self.parameters['runner_mode']

        if self.label is None:
            raise CalculatorSetupError('No calculation label specified.')

        if self.perform_check_successful:
            success, message = check_successful(self.label)

            if not success:
                self.terminated = True
                raise RuntimeError(message)

        if mode == 1:
            results = read_results_mode1(self.label, self._directory)
        elif mode == 2:
            results = read_results_mode2(self.label, self._directory)
        elif mode == 3:
            results = read_results_mode3(self._directory)

        # Add the results to the 'results' dictionary of the calculator.
        self.results.update(results)

        # Store the results as instance properties so they may serve as input
        # parameters in future runs.
        # This also keeps them from being overwritten when changes are detected
        # on the keywords.
        for key, value in results.items():
            self.__dict__[f'{key}'] = value

    def write_input(
        self,
        atoms: Union[Atoms, List[Atoms]],
        properties: Optional[List[str]] = None,
        system_changes: Optional[List[str]] = None
    ) -> None:
        """Write relevant RuNNer input file(s) to the calculation directory.

        Parameters
        ----------
        atoms:
            A single structure or a list of structures for which the symmetry
            functions shall be calculated (= RuNNer Mode 1), for which atomic
            properties like energies and forces will be calculated (= RuNNer
            Mode 3) or on which a neural network potential will be trained
            (= RuNNer Mode 2).
        properties:
            The target properties which shall be returned. See
            `implemented_properties` for a list of options.
        system_changes:
            A list of changes in `atoms` in comparison to the previous run.
        """
        # Per default, `io.write_all_inputs` will only write the input.data and
        # input.nn files (= all that is needed for RuNNer Mode 1).
        # Therefore, all other possible input options are set to `None`.
        scaling = None
        weights = None
        splittraintest = None
        sfvalues = None

        if self.label is None:
            raise CalculatorSetupError('No calculation label specified.')

        # RuNNer Mode 2 additionally requires symmetry function values and the
        # information, which structure within input.data belongs to the training
        # and which to the testing set.
        if self.parameters['runner_mode'] == 2:
            sfvalues = self.sfvalues or self.results['sfvalues']
            splittraintest = (self.splittraintest or
                              self.results['splittraintest'])

        # RuNNer Mode 3 requires the symmetry function scaling data and the
        # neural network weights which were obtained as the results of
        # RuNNer Mode 2.
        elif self.parameters['runner_mode'] == 3:
            scaling = self.scaling or self.results['scaling']
            weights = self.weights or self.results['weights']

        # Call the method from the parent function, so that directories are
        # created automatically.
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

        # Write the relevant files to the calculation directory.
        io.write_all_inputs(atoms, parameters=self.parameters, label=self.label,
                            scaling=scaling, weights=weights,
                            splittraintest=splittraintest, sfvalues=sfvalues,
                            directory=self._directory)

        if self.start_check_progress:
            threading.Timer(self.progressbar_freq, self.check_progress).start()
            self.start_check_progress = False

    def check_progress(self):
        """Update `self.progress_bar` with the status of the calculation.

        This function is first called when writing the input is finished and
        then recursively updated until either the final step is reached
        or the status of `self.terminated` changes.
        """
        filename = self.label + '.out'

        state = self.progress_bar.n
        if os.path.exists(filename):
            if self.parameters['runner_mode'] == 1:
                state = io.check_status_mode1(filename)
            elif self.parameters['runner_mode'] == 2:
                state = io.check_status_mode2(filename)
            elif self.parameters['runner_mode'] == 3:
                state = io.check_status_mode3(filename)

        self.progress_bar.update(state - self.progress_bar.n)

        if state == self.progress_bar.total or self.terminated:
            return

        threading.Timer(self.progressbar_freq, self.check_progress).start()

    def check_state(
        self,
        atoms: Union[Atoms, List[Atoms]],
        tol: float = 1e-15
    ) -> List[str]:
        """Check for any changes since the last calculation.

        Check whether any of the parameters in atoms differs from those in
        `self.atoms`. Overrides the `Calculator` routine to extend the
        functionality to List[Atoms], i.e. RuNNer datasets.

        Parameters
        ----------
        atoms:
            A RuNNer dataset or an ASE Atoms object which will be compared
            to the calculator storage.
        tol:
            The tolerance for float comparisons.

        """
        # If more than one Atoms object is passed, check_state for each `Atoms`.
        if isinstance(atoms, list):

            # If no dataset has been defined yet, no changes will be found.
            if self.dataset is None:
                return []

            system_changes = []
            for idx, structure in enumerate(atoms):
                structure_changes = compare_atoms(structure, self.dataset[idx],
                                                  tol=tol)
                system_changes.append(structure_changes)

            # Unfold the list of found changes and return.
            return [change for structure in system_changes
                    for change in structure]

        return compare_atoms(self.atoms, atoms, tol=tol)

    def get_forces(
        self,
        atoms: Optional[Atoms] = None
    ) -> np.ndarray:
        """Calculate the atomic forces.

        Overrides the `Calculator` routine to ensure that the `calculate_forces`
        keyword is set and RuNNer is in prediction mode (Mode 3).
        Otherwise, RuNNer does not yield atomic forces.

        Parameters
        ----------
        atoms:
            The Atoms object for which the forces will be calculated.

        Returns
        -------
        stress:
            The atomic stress in a [Nx3] array where N is the number of atoms
            in the system.
        """
        self.parameters['runner_mode'] = 3
        self.parameters['calculate_forces'] = True
        return super().get_forces(atoms)

    def get_stress(
        self,
        atoms: Optional[Atoms] = None
    ) -> np.ndarray:
        """Calculate the atomic stress.

        Overrides the `Calculator` routine to ensure that the `calculate_stress`
        keyword is set and RuNNer is in prediction mode (Mode 3).
        Otherwise, RuNNer does not yield stress values.

        Parameters
        ----------
        atoms:
            The Atoms object for which the stress will be calculated.

        Returns
        -------
        stress:
            The atomic stress in a [Nx3x3] array where N is the number of atoms
            in the system.
        """
        self.parameters['runner_mode'] = 3
        self.parameters['calculate_stress'] = True
        return super().get_stress(atoms)

    def get_lammps_potential(
        self,
        elements: Optional[List[str]] = None,
        showew: bool = True,
        showewsum: int = 0,
        resetew: bool = False,
        maxew: int = 100
    ) -> List[str]:
        """Return the LAMMPS pair_coeff and pair_style for this potential.

        When using RuNNer with the ML-HDNNP package in LAMMPS, one needs
        to set a pair_style and corresponding pair_coeff. This function
        takes care of tailoring these settings to the current potential.

        Parameters
        ----------
        elements:
            The elements which are used in the simulation. These have to be
            a direct between the LAMMPS atoms and the elements in input.nn.
            Be very careful: if this mapping is wrong, it can happen that you
            are suddenly not simulating H2O but O2H, which will usually
            immediately terminate with an error.
        showew:
            Whether to show extrapolation warnings.
        showewsum:
            Show a summary of extrapolation warnings after this many timesteps.
        resetew:
            Reset extrapolation warnings after each timestep.
        maxew:
            Total maximum number of extrapolation warnings before the simulation
            terminates with an error.

        Returns
        -------
        list:
            A two-entry list, the first one is the pair_style string, the second
            one the is the pair_coeff string.
        """
        # First, check whether the fit is finished.
        if self.weights is None or self.scaling is None:
            raise RuntimeError('This potential has not been trained yet.')

        # If no elements were provided, we assume the simulation contains
        # all elements in the training dataset.
        if elements is None:
            elements = self.elements

        # Save the mapping of elements between LAMMPS and n2p2.
        emap = ' '.join(el for el in elements)

        # Get the cutoff radius of the symmetry functions.
        cutoffs = self.parameters['symfunction_short'].cutoffs
        cutoff = cutoffs[0]
        if len(cutoffs) > 1:
            raise RuntimeError(
                'LAMMPS potential can only be generated for a '
                + 'uniform cutoff radius.'
            )

        return [
            f'pair_style hdnnp {cutoff / 1.8897261328} dir "{self.directory}" '
            + f'showew {"yes" if showew else "no"} '
            + f'showewsum {showewsum} '
            + f'resetew {"yes" if resetew else "no"} '
            + f'maxew {maxew} '
            + 'cflength 1.8897261328 cfenergy 0.0367493254',
            f'pair_coeff * * {emap}',
        ]

    def sample_dataset(
        self,
        rng: Optional[np.random.Generator] = None,
        seed: int = 42,
        num_batches: int = 1,
        size_per_batch: int = 100
    ) -> List[List[Atoms]]:
        """Sample a random batch of structures from `self.dataset`.

        Parameters
        ----------
        rng:
            A random number generator for reproducible results.
        seed:
            If no `rng` is given, a new one is created with the given seed.
        num_batches:
            Number of batches that will be generated.
        size_per_batch:
            The size of each batch.

        Returns
        -------
        batches:
            A `num_batches`-size list of batches, each containing
            `size_per_batch` structures.
        """
        if self.dataset is None:
            raise RuntimeError('Please attach a dataset to the calculator '
                               'to sample batches.')

        if rng is None:
            rng = np.random.default_rng(seed)

        num_strucs = len(self.dataset)

        batches = []
        for _ in range(num_batches):
            indices = rng.choice(
                range(num_strucs),
                size_per_batch,
                replace=False
            )

            batch = [self.dataset[i] for i in indices]
            batches.append(batch)

        return batches

    def filter_dataset(
        self,
        filter_function: Callable[[Atoms], bool],
        **kwargs: Dict[Any, Any]
    ) -> List[Atoms]:
        """Filter `self.dataset` according to the `filter_function`.

        The function merely returns a new dataset. `self.dataset` is
        not changed by this function!

        Parameters
        ----------
        filter_function:
            A function that takes, as its first argument, an Atoms object
            and returns True or False, depending on whether the structure
            passes the filter or not.
        kwargs:
            Additional parameters for `filter_function` All `kwargs` are
            passed on to the filter function.

        Returns
        -------
        filtered_data:
            A list of all structures which passed the filter.

        """
        if self.dataset is None:
            raise RuntimeError('Please attach a dataset to the calculator '
                               'to sample batches.')

        filtered_data: List[Atoms] = []
        for structure in self.dataset:
            if filter_function(structure, **kwargs):
                filtered_data.append(structure)

        return filtered_data
