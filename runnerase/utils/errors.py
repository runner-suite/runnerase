#!/usr/bin/env python3
# encoding: utf-8
"""Custom error functions for runnerase."""

from typing import Optional


class UnrecognizedKeywordError(Exception):
    """Error class for marking format mistakes in input.nn parameter files."""

    def __init__(self, keyname: str) -> None:
        """Initialize exception with custom error message."""
        super().__init__(f"The keyword {keyname} is not recognized.")


class FileFormatError(Exception):
    """Generic error class for marking format mistakes in RuNNer files."""

    def __init__(self, message: Optional[str] = None) -> None:
        """Initialize exception with custom error message."""
        if not message:
            message = "File formatted incorrectly."

        super().__init__(message)
