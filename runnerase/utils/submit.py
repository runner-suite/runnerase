"""Functions for calculation submission to queueing systems.

Provides
--------

submit_slurm:
    Submit an arbitrary standalone or class-bound function to
    the Slurm queueing system (others might work, not tested).
slurm_barrier:
    Hangs until a (list of) calculation(s) has finished/aborted.

"""

from typing import Dict, Any, Union, List, Callable
import os
import subprocess
import pickle


# Default settings when submitting a runnerase calculation.
DEFAULT_SUBMIT_DICT: Dict[str, Any] = {
    'job_name': 'runnerase',
    'output_name': 'slurm-%j.out',
    'num_cores': None,
    'memory_max': '4G',
    'queue_name': None,
    'runtime_max': '01-00:00:00',
    'working_directory': None,
    'command': None
}


def slurm_barrier(job_ids: Union[List[int], int]) -> None:
    """Hangs until the calculations `job_ids` have finished.

    Internally, it calls `squeue` to find out whether the calculations
    are still active in the queue.

    !!! warning "Be warned:"
        If you modified the `squeue` command on your system, this
        might not work correctly anymore!
    """
    running = True

    if isinstance(job_ids, int):
        job_ids = [job_ids]

    while running:
        running = any(_check_slurm_queue(i) for i in job_ids)


def _check_slurm_queue(target_id: int) -> bool:
    """Check whether `target_id` is in the Slurm queue."""
    job_table = subprocess.check_output(["squeue"])
    job_table_str = job_table.decode('utf-8')
    job_lines = job_table_str.split('\n')[1:]

    for line in job_lines:

        if len(line) != 0:
            job_id = int(line.split()[0])

            if job_id == target_id:
                return True

    return False


# pylint: disable=too-many-locals
# Wrapper function that defines a whole workflow. Such a thing usually needs
# several local variables.
def submit_slurm(
    function: Callable[..., Any],
    *args,
    **kwargs
) -> int:
    """Submit a `function` with `args` and `kwargs` to the Slurm queue.

    The function wraps any other function and makes it queue-submittable.
    It achieves this in key three steps:
    1. Wrap the function and its arguments in a dictionary and pickle this
       dict to a file.
    2. Write a python script that can load and execute the function from
       the pickle file.
    3. Write a bash script than executes the Python function
    4. Submit the job with `sbatch`.

    A template for the bash script has to be provided by the user in advance.
    It can be put into a $HOME/.runnerase/<queue_name>.sh file for every
    queue to which the user wants to submit a calculation. Alternatively,
    it can be put into the directory, where the script which calls
    `submit_slurm` is, too. The local script always takes precedence over the
    one in the .runnerase directory.

    In the template script, the submission settings can either be hardcoded or
    variables can be wrapped in "{{variable_name}}" tags (without the quotes).
    All variables in this format will be replaced by the values provided in the
    submission dictionary (given they exist in the dictionary). The default
    parameters of `submit_dict` are given by `DEFAULT_SUBMIT_DICT`.

    The only required submission parameters are `num_cores` and `queue_name`.

    Parameters
    ----------
    function:
        An function with an arbitrary interface. Any required parameters
        must be given as `args` or `kwargs`.
    args:
        Non-keyword arguments, are passed on to `function`.
    kwargs:
        Keyword arguments, are either used to update `submit_dict` or passed on
        to `function`. This also means that no function argument can have the
        same name as a key in the submit_dict.

    Returns
    -------
    job_id:
        The Slurm ID of the job that was submitted.

    """
    # Find out the next valid index we can use for this calculation
    # without overwriting anything.
    cwd = os.getcwd()
    calc_idx = _get_next_calculation_id(cwd)

    # Whether to clean up after the calculation or not.
    cleanup = True
    if 'cleanup' in kwargs:
        cleanup = bool(kwargs.pop('cleanup'))

    # Define paths for temporary files.
    python_script_path = f'.calculation_{calc_idx}.py'
    pickle_path = f'.calculation_{calc_idx}.pkl'
    slurm_script_path = f'.work_{calc_idx}.sh'

    # Define defaults parameters.
    submit_dict = DEFAULT_SUBMIT_DICT.copy()
    submit_dict.update({
        'working_directory': cwd,
        'command': f'python {python_script_path}'
    })

    # Extract user settings.
    for key in submit_dict:
        if key in kwargs:
            submit_dict[key] = kwargs.pop(key)

    # Check if all required parameters were given.
    if submit_dict['num_cores'] is None or submit_dict['queue_name'] is None:
        raise RuntimeError('When submitting to the queue, '
                           'queue_name and num_cores must be set.')

    # Pickle the whole input and its arguments.
    pickle_dict = {
        'function': function,
        'args': args,
        'kwargs': kwargs
    }
    with open(pickle_path, 'wb') as outfile:
        pickle.dump(pickle_dict, outfile)

    # Write the python script to run the job.
    with open(python_script_path, 'wt', encoding='utf-8') as outfile:
        script = _write_calculation_script(pickle_path)
        outfile.write(script)

    # Write the slurm submission script.
    _prepare_slurm_submission_script(
        cwd,
        slurm_script_path,
        pickle_path,
        python_script_path,
        submit_dict,
        cleanup=cleanup
    )

    # Call sbatch and submit the job.
    job_out = subprocess.check_output(["sbatch", slurm_script_path])

    # Return job id.
    job_id = int(job_out.split()[3].strip())
    return job_id


# pylint: disable=too-many-arguments
# All files that are cleaned up need to be passed to this function. Could
# also be done as an array but this way is easier to understand imho.
def _prepare_slurm_submission_script(
    cwd: str,
    output_path: str,
    pickle_path: str,
    python_script_path: str,
    submit_dict: Dict[str, Any],
    cleanup: bool = True
) -> None:
    """Create a submission script with all commands to submit a calculation.

    This function loads the slurm submission template provided by the user
    (.sh script) and fills the placeholders in it with the values given in
    `submit_dict`.

    Parameters
    ----------
    cwd:
        The current working directory.
    output_path:
        The path to which the final script will be written.
    pickle_path:
        The path to the pickle file containing the function and its arguments.
    python_script_path:
        The file to the Python script that loads and executes the function under
        `pickle_path`.
    submit_dict:
        Dictionary containing all values to fill the placeholders in the
        submission script.
    cleanup:
        Whether to remove the Python script, the pickled function, and the
        submission script itself after the calculation has returned
        (successful or not).

    """
    # Load and fill the template script.
    template_path = _get_slurm_template_path(cwd, submit_dict['queue_name'])
    script = _slurm_fill_placeholders(template_path, submit_dict)

    # Add cleanup section if desired by the user.
    if cleanup:
        script += f"""
# Remove temporary submission files from runnerase.
rm {pickle_path}
rm {python_script_path}
rm {output_path}
"""

    # Write the final script to disk.
    with open(output_path, 'wt', encoding='utf-8') as outfile:
        outfile.write(script)


def _slurm_fill_placeholders(
    template_path: str,
    submit_dict: Dict[str, Any]
) -> str:
    """Fill all placeholders in the slurm template file with values.

    In the template file provided by the user, variables like the number of
    cores or the time limit are marked by "{{variable_name}}" tags (without
    the quotes). Here, they are replaced by the values in `submit_dict`.

    Parameters
    ----------
    template_path:
        Relative or absolute path to the template file.
    submit_dict:
        Dictionary that provides a mapping between the placeholders in the
        template file and the desired values.

    Returns
    -------
    template:
        A full string of the loaded and filled-in template file.

    """
    with open(template_path, 'rt', encoding='utf-8') as infile:
        template = infile.read()

    for key, val in submit_dict.items():
        template = template.replace(f'{{{{{key}}}}}', str(val))

    return template


def _get_next_calculation_id(cwd: str) -> int:
    """Find next free calculation index from .calculation files in `cwd`.

    Based on the .calculation_<index> files in the current working directory,
    return the highest index + 1 as the index for the next calculation.
    This prevents overwriting possibly needed files.

    Parameters
    ----------
    cwd:
        The current working directory.

    Returns
    -------
    highest_idx:
        The next possible index for a new calculation.
    """
    highest_idx = 0
    for file in os.listdir(cwd):
        if '.calculation_' in file:
            idx = int(file.split('_')[1].split('.')[0])

            highest_idx = max(highest_idx, idx)

    return highest_idx + 1


def _get_slurm_template_path(cwd: str, queue_name: str) -> str:
    """Find the Slurm submission script template created by the user.

    The script template either resides in the .runnerase folder in the
    users $HOME path or directly in the calculation folder.

    Parameters
    ----------
    queue_name:
        The name of the queue for which the template is searched.

    Returns
    -------
    path:
        Either the path to the local "queue_name.sh" or the one in
        ~/.runnerase/ . The local file always takes precendence.
    """
    home = os.path.expanduser("~")

    local_path = os.path.join(cwd, f'{queue_name}.sh')
    home_path = os.path.join(home, '.runnerase', f'{queue_name}.sh')

    paths = [local_path, home_path]

    # Local work_template.sh takes precedence.
    for path in paths:
        if os.path.exists(path):
            return path

    raise RuntimeError(f'No slurm template found for queue {queue_name}.')


def _write_calculation_script(
    pickle_path: str
) -> str:
    """Write a Python script executing a previously pickled Python function.

    The script loads a Python function and its args and kwargs from a pickle
    file and then runs the function.
    """
    return f"""
#!/usr/bin/env python3
# encoding: utf-8

import os
from runnerase import *
import pickle

# Load the written configuration file.
with open('{pickle_path}', 'rb') as infile:
    pickle_dict = pickle.load(infile)

function = pickle_dict['function']
args = pickle_dict['args']
kwargs = pickle_dict['kwargs']

# Execute the function.
function(*args, **kwargs)
"""
