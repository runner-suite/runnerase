"""Default dictionaries of RuNNer parameters.

Attributes
----------
DEFAULT_TRAINING_PARAMETERS:
    A nested dict containing default settings for five training properties:
        - symmetry functions
        - dataset filter
        - mode 1
        - mode 2
        - mode 3
DEFAULT_PARAMETERS:
    A selection of those keywords in `RUNNERCONFIG_DEFAULTS` which are
        - either mandatory for RuNNer usage,
        - or very commonly used,
        - or have a default in RuNNer that is rarely seen in applications.
    This dictionary is used when initializing a RuNNer calculator object.
"""

from typing import Dict, List
from runnerase.symmetry_functions import SymmetryFunctionSet
from .atoms import default_filter_function

DEFAULT_PARAMETERS: Dict[str, object] = {
    # General for all modes.
    'runner_mode': 1,                     # Default is starting a new fit.
    # All modes.
    'symfunction_short': SymmetryFunctionSet(),  # Auto-set if net provided.
    'elements': None,                     # Auto-set by `set_atoms()`.
    'number_of_elements': 0,              # Auto-set by `set_atoms()`.
    'bond_threshold': 0.5,                # Default OK but system-dependent.
    'nn_type_short': 1,                   # Most people use atomic NNs.
    # 'nnp_gen': 2,                        # 2Gs remain the most common usage.
    'use_short_nn': True,                 # Short-range fitting is the default.
    'optmode_charge': 1,                  # Default OK but option is relevant.
    'optmode_short_energy': 1,            # Default OK but option is relevant.
    'optmode_short_force': 1,             # Default OK but option is relevant.
    'points_in_memory': 1000,             # Default value is legacy.
    'cutoff_type': 1,                     # Default OK, but important.
    # Mode 1.
    'test_fraction': 0.1,                 # Default too small, more common.
    # Mode 1 and 2.
    'use_short_forces': True,             # Force fitting is standard.
    # Mode 2.
    'epochs': 30,                         # Default is 0, 30 is common.
    'kalman_lambda_short': 0.98000,       # No Default, this is sensible value.
    'kalman_nue_short': 0.99870,          # No Default, this is sensible value.
    'mix_all_points': True,               # Standard option.
    'nguyen_widrow_weights_short': True,  # Typically improves the fit.
    'repeated_energy_update': True,       # Default is False, but usage common.
    'short_energy_error_threshold': 0.1,  # Use only energies > 0.1*RMSE.
    'short_energy_fraction': 1.0,         # All energies are used.
    'short_force_error_threshold': 1.0,   # All forces are used.
    'short_force_fraction': 0.1,          # 10% of the forces are used.
    'use_old_weights_charge': False,      # Relevant for calculation restart.
    'use_old_weights_short': False,       # Relevant for calculation restart.
    'write_weights_epoch': 1,             # Default is 1, very verbose.
    'force_update_scaling': 1.0,          # Force & energy have same weight.
    'scale_symmetry_functions': True,     # Scaling is used by almost everyone.
    'center_symmetry_functions': True,    # This is standard procedure.
    'precondition_weights': True,         # This is standard procedure.
    # Mode 2 and 3.
    'global_activation_short': [['t', 't', 'l']],  # tanh / linear activ. func.
    'global_hidden_layers_short': 2,      # 2 hidden layers
    'global_nodes_short': [[15, 15]],       # 15 nodes per hidden layer.
    # Mode 3.
    'calculate_forces': True
}


DEFAULT_TRAINING_PARAMETERS = {
    'symfuns': {
        'radial': {
            'algorithm': 'turn',
            'amount': 5,
            'cutoff': 12.0
        },
        'angular': {
            'algorithm': 'literature',
            'amount': 4,
            'cutoff': 12.0
        }
    },
    'filter': {
        'function': default_filter_function,
        'kwargs': {
            'cutoff': 12.0
        }
    },
    'mode1': {'runner_mode': 1},
    'mode2': {'runner_mode': 2},
    'mode3': {'runner_mode': 3}
}


RUNNERDATA_KEYWORDS: List[str] = ['begin', 'comment', 'lattice', 'atom',
                                  'charge', 'energy', 'end']
