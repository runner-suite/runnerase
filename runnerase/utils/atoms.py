#!/usr/bin/env python3
# encoding: utf-8
"""Utility Functions for Atoms objects.

Provides
--------
- extract_environment: Extract the atomic environment around an atom.
- optimize_atomic_energies: Get the optimal atomic energies per element.
- binary_composition: Find the percentage of one element in a structure.
- get_elements: Retrieve all elements in a structure.
- get_element_groups: Group elements by pairs or triplets.
- get_minimum_distances: Get the minimal distance for each element pair in
    a structure.
- get_triplets:
    Get all triplets that appear in a dataset within a given cutoff radius.
    Warning: very slow.
- default_filter_function: A filter function to generate more sensible
    training datasets.
- count_ghost_cells: Count the number of times a structure has to be
    repeated to cover a given cutoff radius.
- repeat: Repeat a structure a given number of times, keeping the original
    structure right in the middle of the new structure.

"""

from typing import List, Dict, Tuple, Optional

from itertools import combinations_with_replacement, product

import numpy as np
from scipy.optimize import minimize

from ase.geometry import get_distances
from ase.atoms import Atoms
from ase.data import atomic_numbers
from ase.units import Bohr


# pylint: disable=too-many-locals
# Reason: Many boundary conditions have to be checked.
def extract_environment(
    atoms: Atoms,
    atom_idx: int,
    cutoff: np.ndarray,
    severity: Optional[float] = None,
    margin: float = 0.0
) -> Atoms:
    """Extract the environment around `atom_idx` in `atoms`.

    The function cuts the atomic environment around an atom from a
    trajectory step. It supports ellipsoid cutoff radii (which are
    different in x, y and z direction) and also supports cutting
    a cubic box around the atomic environment with a margin (useful
    for equilibration). If the cutoff radius extends over multiple
    images of the cell, the cell is repeated as needed.

    Parameters
    ----------
    atoms:
        The structure from which the environment is cut.
    atom_idx:
        The atom in `atoms` around which the environment is
        extracted.
    cutoff:
        The cutoff radius in x, y and z direction that is considered
        as the atomic environment.
    severity:
        Severity of the extrapolation (for coloring and selection later
        on).
    margin:
        If given, an additional cubic box is cut around the extracted
        cutoff radius. This can be very useful for equilibration of
        periodic boundary conditions around a spherical atomic
        environment.
    Returns
    -------
    environment:
        The extracted atomic environment, possibly including the
        atoms within the additional `margin`.
    """
    supercell = atoms.copy()

    if any(supercell.pbc):
        repeats = count_ghost_cells(supercell, cutoff + margin)
        supercell = repeat(supercell, repeats)

    atom_pos = supercell[atom_idx].position

    if margin == 0.0:
        ellipsoid = np.sum(
            ((supercell.positions - atom_pos) / cutoff)**2,
            axis=1
        )

        boundary_condition = ellipsoid < 1.0

    else:
        half_len_cube = cutoff + margin
        xenv = supercell.positions[:, 0]
        yenv = supercell.positions[:, 1]
        zenv = supercell.positions[:, 2]
        x = atom_pos[0]
        y = atom_pos[1]
        z = atom_pos[2]

        boundary_condition = np.logical_and.reduce(
            (
                xenv < x + half_len_cube[0],
                xenv > x - half_len_cube[0],
                yenv < y + half_len_cube[1],
                yenv > y - half_len_cube[1],
                zenv < z + half_len_cube[2],
                zenv > z - half_len_cube[2]
            ),
            dtype=bool
        )

    # Choose new positions.
    positions = supercell.positions[boundary_condition]
    symbols = supercell.symbols[boundary_condition]

    # Create new atoms object.
    environment = Atoms(symbols, positions)

    if severity is not None:
        environment.arrays['Severity'] = np.zeros(len(environment))
        environment.arrays['Severity'][atom_idx] = severity

    # Move atoms to origin.
    com = environment.get_center_of_mass()
    environment.positions -= com

    return environment


def optimize_atomic_energies(atoms: Atoms) -> Dict[str, float]:
    """Optimize a set of energies to be close to the total energy of `atoms`.

    For each element in `atoms`, return one atomic energy. The sum of
    all atomic energies over the atoms in `atoms` approaches the total energy
    of atoms.

    Parameters
    ----------
    atoms:
        The structure for whose elements the energies are determined.

    Returns
    -------
    atomic_energies:
        A dictionary where the keys are the element and the values are the
        corresponding atomic energy.
    """
    # Get element occurences the `struc`.
    energy = atoms.get_potential_energy()
    elems = atoms.symbols
    element_occurences = {i: elems.count(i) for i in set(elems)}

    # For the optimization, elements are weighed by how often they occur.
    weight = np.array(list(element_occurences.values()))

    # Optimize energy.
    opt_result = minimize(
        lambda a, b, c: abs(np.sum(a * b) - c),
        x0=[1e4 for i in range(len(element_occurences))],
        args=(weight, energy,)
    )

    # Sort atomic energies into a dictionary.
    atomic_energies = {}
    for idx, elem in enumerate(element_occurences.keys()):
        atomic_energies[elem] = opt_result.x[idx]

    return atomic_energies


def binary_composition(atoms: Atoms, element: str) -> float:
    """Calculate the `element` content in `atoms`."""
    return atoms.symbols.count(element) / len(atoms)


def get_elements(
    images: List[Atoms],
    sort_by_appearance: bool = False
) -> List[str]:
    """Extract a list of elements from a given list of ASE Atoms objects.

    Parameters
    ----------
    images:
        A list of ASE atoms objects.
    sort_by_appearance:
        Whether to sort the elements by order of appearance in images.
        Default is to sort by atomic number.

    Returns
    -------
    elements:
        A list of all elements contained in `images`.

    """
    # Get the chemical symbol of all elements.
    elements: List[str] = []
    for atoms in images:
        for element in atoms.get_chemical_symbols():
            elements.append(element)

    # Remove repeated elements.
    elements = list(sorted(set(elements), key=elements.index))

    # Sort the elements by atomic number.
    if not sort_by_appearance:
        elements.sort(key=lambda i: atomic_numbers[i])

    return elements


def get_element_groups(
    elements: List[str],
    groupsize: int
) -> List[List[str]]:
    """Create doubles or triplets of elements from all `elements`.

    Parameters
    ----------
    elements:
        A list of all the elements from which the groups shall be built.
    groupsize:
        The desired size of the group.

    Returns
    -------
    groups:
        A list of elements groups.
    """
    # Build pairs of elements.
    if groupsize == 2:
        doubles = list(product(elements, repeat=2))
        groups = [[a, b] for (a, b) in doubles]

    # Build triples of elements.
    elif groupsize == 3:
        pairs = combinations_with_replacement(elements, 2)
        triples = product(elements, pairs)
        groups = [[a, b, c] for a, (b, c) in triples]

    return groups


def get_minimum_distances(
    dataset: List[Atoms],
    elements: List[str]
) -> Dict[str, float]:
    """Calculate min. distance between all `elements` pairs in `dataset`.

    Parameters
    ----------
    dataset:
        The minimum distances will be returned for each element pair across all
        images in `dataset`.
    elements:
        The list of elements from which a list of element pairs will be built.

    Returns
    -------
    minimum_distances: Dict[str, float]
        A dictionary where the keys are strings of the format 'C-H' and the
        values are the minimum distances of the respective element pair.

    """
    minimum_distances: Dict[str, float] = {}
    for elem1, elem2 in get_element_groups(elements, 2):
        for structure in dataset:

            elems = structure.get_chemical_symbols()

            # All positions of one element.
            pos1 = structure.positions[np.array(elems) == elem1]
            pos2 = structure.positions[np.array(elems) == elem2]

            # If there is only one atom of this element, skip.
            if (
                (pos1.shape[0] == 0) or
                (pos2.shape[0] == 0) or
                (pos1.shape[0] <= 1 and pos2.shape[0] <= 1)
            ):
                continue

            distmatrix = get_distances(pos1, pos2)[1]

            # Remove same atom interaction.
            flat = distmatrix.flatten()
            flat = flat[flat > 0.0]

            dmin: float = min(flat)
            label = '-'.join([elem1, elem2])

            if label not in minimum_distances:
                minimum_distances[label] = dmin

            # Overwrite the currently saved minimum distances if a smaller one
            # has been found.
            if minimum_distances[label] > dmin:
                minimum_distances[label] = dmin

    return minimum_distances


# pylint: disable=too-many-locals
# Reason: Keeping track of triplets needs many variables. Cannot think of
# a smarter way yet, sorry pylint
def get_triplets(
    elements: List[str],
    dataset: List[Atoms],
    cutoff: float
) -> List[str]:
    """Calculate all triplets of `elements` in `dataset` within `cutoff`.

    Iterates over all structures in the dataset and searches for the possible
    combinations of triplets given be `elements`.
    This function can be very slow for large datasets.

    Parameters
    ----------
    elements:
        All possible triplet combinations of these chemical elements are
        searched in the dataset.
    dataset:
        Structures in which the triplets are searched.
    cutoff:
        Radius in which the pairwise distances how to lie to be counted as a
        triplet.
    Returns
    -------
    triplets:
        A unique list of all triplets of elements which actually appear in the
        dataset. The format is 'H-C-Ne'.

    """
    triplets = []
    i = 0
    for structure in dataset:
        i += 1
        for atom1 in structure:
            for atom2 in structure:
                dist = atom1.position - atom2.position

        elems = np.array(structure.get_chemical_symbols())

        # Calculate all pairwise distances within the cutoff radius.
        dist = structure.get_all_distances()
        inside_cutoff = np.where(dist > cutoff * Bohr, 0, 1)

        for triplet in get_element_groups(elements, 3):
            elem1, elem2, elem3 = triplet

            # Filter pairs ij and ik for the right element.
            d_ij = inside_cutoff[elems == elem1][:, elems == elem2]
            d_ik = inside_cutoff[elems == elem1][:, elems == elem3]

            # Determine if atoms with the element combinations ij and ik have
            # neighbors are not.
            num_neighbors_ij = np.sum(d_ij, axis=1)
            num_neighbors_ik = np.sum(d_ik, axis=1)

            hasneighbors_ij = np.where(num_neighbors_ij == 0, False, True)
            hasneighbors_ik = np.where(num_neighbors_ik == 0, False, True)

            # If both element pairs ij and ik have at least one neighbor within
            # the cutoff radius, we assume that the triplet is needed.
            if any(i for i in np.logical_and(hasneighbors_ij, hasneighbors_ik)):
                triplets.append('-'.join(triplet))

    return list(set(triplets))


# pylint: disable=too-many-locals
# Reason: This wrapper routine checks many conditions at once,
# so it needs many variables.
def default_filter_function(
    atoms: Atoms,
    cutoff: float,
    upper_boundary_max_force: float = 50.0,
    upper_boundary_energy_per_atom: float = 0.0,
    upper_boundary_volume_per_atom: float = 30.0
) -> bool:
    """Filter a given dataset for structures unsuited for RuNNer.

    The default filter criteria are:
    - maximum force component (default filters very high-force structures)
    - maximum energy per atom (default filters non-binding structures)
    - maximum volume per atom (default filters very isolated structures)
    - atoms without neighbors are filtered
    - structures with nan force values are filtered.

    Parameters
    ----------
    atoms:
        The chemical structure that is filtered.
    cutoff:
        The cutoff radius set by the user.
    upper_boundary_max_force:
        The maximum force component allowed in `atoms`.
    upper_boundary_energy_per_atom:
        The maximum total energy (units per atom) allowed.
    upper_boundary_volume_per_atom:
        The maximum volume per atom allowed.

    Returns
    -------
    result:
        True if the structure passes the tests, otherwise False.
    """
    # Get the total energy and atomic positions.
    energy = atoms.get_potential_energy()
    forces = atoms.get_forces()
    positions = atoms.positions
    natoms = len(atoms)

    # Normalize energy by number of atoms
    energy /= natoms

    # Get the absolute maximum force component.
    fmax = abs(forces).max()

    # Build the distance matrix.
    distmatrix = get_distances(positions, positions)[1]

    # Check if every atom has at least one neighbor in a 12 Bohr = 6.35 Ang
    # cutoff radius.
    no_neighbors = False
    for _, atom in enumerate(distmatrix):

        # Remove self interaction.
        atom_no_selfinteraction = atom[atom > 0.0]

        if all(atom_no_selfinteraction > cutoff * Bohr):
            no_neighbors = True

    # Filter based on density.
    dens = atoms.get_volume() / natoms

    return (fmax < upper_boundary_max_force
            and energy <= upper_boundary_energy_per_atom
            and no_neighbors is False
            and not np.isnan(forces).any()
            and dens < upper_boundary_volume_per_atom)


def count_ghost_cells(
    atoms: Atoms,
    cutoff: np.ndarray
) -> Tuple[int, int, int]:
    """Get num of repeats of `atoms` needed to cover `cutoff`."""
    if not any(atoms.pbc):
        return (1, 1, 1)

    a = atoms.cell[0, :]
    b = atoms.cell[1, :]
    c = atoms.cell[2, :]

    # Calculate the norm vectors of the 3 planes.
    axb = np.cross(a, b)
    axb = axb / np.sqrt(sum(axb**2))
    axc = np.cross(a, c)
    axc = axc / np.sqrt(sum(axc**2))
    bxc = np.cross(b, c)
    bxc = bxc / np.sqrt(sum(bxc**2))

    # Calculate the projections.
    proja = abs(sum(a * bxc))
    projb = abs(sum(b * axc))
    projc = abs(sum(c * axb))

    # Determine how often the cell needs to be multiplied.
    ra = cutoff[0] / proja
    rb = cutoff[1] / projb
    rc = cutoff[2] / projc
    num_x = int(ra) + 1
    num_y = int(rb) + 1
    num_z = int(rc) + 1
    return (num_x, num_y, num_z)


# pylint: disable=too-many-locals
# Reason: The variables are very repetitive because we have to keep
# track of three cell axes and spatial dimensions.
def repeat(atoms: Atoms, nreps: Tuple[int, int, int]) -> Atoms:
    """Repeat `atoms` `nreps` times along each Cartesian axes.

    In contrast to ASEs `repeat` function, this procedure leaves
    the original cell in the center of the supercell. In other words,
    the ghost cells are generated _around_ the zeroth image. In ASE,
    the original cell ends up in the lower left corner of the supercell.
    """
    repeated_atoms = atoms.copy()

    a = atoms.cell[0, :]
    b = atoms.cell[1, :]
    c = atoms.cell[2, :]

    # Expand all arrays to the correct size.
    # The original arrays are all repeated `number_of_ghost_cells`
    # times.
    tot_num_ghost_cells = int(np.prod([2 * i + 1 for i in nreps]))
    for name, arr in repeated_atoms.arrays.items():
        repeated_atoms.arrays[name] = np.tile(
            arr,
            (tot_num_ghost_cells,) + (1,) * (len(arr.shape) - 1)
        )

    natoms = len(atoms)
    positions = repeated_atoms.arrays['positions']
    i0 = natoms  # First natoms entries are the 0th image.
    for n1 in range(-nreps[0], nreps[0] + 1):
        for n2 in range(-nreps[1], nreps[2] + 1):
            for n3 in range(-nreps[2], nreps[2] + 1):

                # Skip 0th image, it is already in positions[0, natoms].
                if n1 == 0 and n2 == 0 and n3 == 0:
                    continue

                i1 = i0 + natoms
                positions[i0:i1] += n1 * a + n2 * b + n3 * c
                i0 = i1

    return repeated_atoms
