#!/usr/bin/env python3
# encoding: utf-8
"""Utility Functions for dictionaries."""

from typing import Dict, Any
from collections.abc import Mapping


def update(
    olddict: Dict[Any, Any],
    updatedict: Mapping[Any, Any]
) -> Dict[Any, Any]:
    """Recursively update dictionary `olddict` with dictionary `updatedict`.

    The intrinsic dict.update() routine is not recursive.
    """
    for k, v in updatedict.items():
        if isinstance(v, Mapping):
            olddict[k] = update(olddict.get(k, {}), v)
        else:
            olddict[k] = v
    return olddict
