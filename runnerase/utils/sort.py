#!/usr/bin/env python3
# encoding: utf-8
"""Utility Functions for sorting."""

from typing import List, Union

import re


def natsort(string: str) -> List[Union[str, int]]:
    """Sorting key for a list of strings, preservering natural ordering.

    Example
    -------
    This is usually used in the context of the intrinsic sorted function:

    >>> x = ['dir_0', 'dir_03', 'dir_1']
    >>> sorted(x, key=natsort)
    Returns ['dir_0', 'dir_1', 'dir_03']

    """
    res: List[Union[str, int]] = []
    for t in re.split(r'(\d+)', string):
        if t.isdigit():
            res.append(int(t))
        else:
            res.append(t.lower())

    return res
