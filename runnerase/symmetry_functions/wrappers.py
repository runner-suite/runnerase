"""Implementation of wrapper for generating parametrized symmetry functions."""

from typing import Optional, Dict, List, Union

from ase.atoms import Atoms

from runnerase.utils.atoms import get_elements

from .symmetryfunctions import SymmetryFunctionSet
from .radial import generate_symmetryfunctions_sftype2
from .angular import generate_symmetryfunctions_sftype3


# This wrapper needs all the possible symmetry function arguments.
# pylint: disable=R0913
def generate_symmetryfunctions(
    dataset: List[Atoms],
    sftype: int = 2,
    cutoff: float = 10.0,
    amount: int = 6,
    algorithm: str = 'half',
    elements: Optional[List[str]] = None,
    min_distances: Optional[Dict[str, float]] = None,
    lambda_angular: Optional[List[float]] = None,
    eta_angular: Optional[Union[Dict[str, float], SymmetryFunctionSet]] = None,
    determine_triplets: bool = False
) -> SymmetryFunctionSet:
    """Generate a set of radial or angular symmetry functions.

    Parameters
    ----------
    dataset:
        A list of structures to which the symmetry function parameters will be
        adapted.
    sftype:
        The type of symmetry function. At the moment, only types `2` and `3` are
        supported.
    cutoff:
        Symmetry function cutoff radius.
    amount:
        The number of symmetry function to be generated for each pair/triplet
        of elements.
    algorithm:
        The algorithm for determination of either the `eta` parameter of the
        radial symmetry functions or the `zeta` parameter of the angular
        symmetry functions. Can be 'half' or 'turn' for radial SFs and 'half',
        'turn', 'literature', or 'eta_mean' for angular symmetry functions.
    elements:
        A list of elements. If not supplied the elements will be determined
        from `dataset` instead. Symmetry functions will always be generated
        for all possible combinations of `element`s.
    min_distances:
        A dictionary that contains the minimum distances between atoms
        belonging to an element pair, e.g. {'C-H': 1.2}. This is required for
        the parametrization of radial symmetry functions.
    lambda_angular:
        A set of lambda parameters for the angular symmetry functions. If not
        supplied, the default [-1.0, +1.0] will be set.
    lambda_angular:
        A set of lambda parameters for the angular symmetry functions. If not
        supplied, the default [-1.0, +1.0] will be set.
    eta_angular:
        For each `eta` in `eta_angular`, one separate set of angular symmetry
        functions will be generated.
        When `algorithm` == 'eta_mean' is set and a set of radial symmetry
        functions is supplied with `eta_angular`, two sets of angular symmetry
        functions will be generated: one with the parameter `eta` set to 0.0 and
        one with the parameter `eta` equal to the mean of the radial symmetry
        function `eta` parameters for all pairs in the current triplet of atoms.
    determine_triplets:
        Whether to explicitely find out which triplets exist in `dataset` within
        `cutoff`. Can take a long time.

    Returns
    -------
    parent_symfunset:
        A set of newly created symmetry functions.
    """
    # If no elements were provided use all elements of this set.
    if elements is None:
        elements = get_elements(dataset)

    # Generate the parent symmetry function set.
    parent_symfunset = SymmetryFunctionSet()

    # Generate radial symmetry functions.
    if sftype == 2:
        generate_symmetryfunctions_sftype2(parent_symfunset, dataset, cutoff,
                                           amount, algorithm, elements,
                                           min_distances)

    # Generate angular symmetry functions.
    elif sftype == 3:
        generate_symmetryfunctions_sftype3(
            parent_symfunset,
            elements,
            dataset if determine_triplets else None,
            cutoff,
            amount,
            algorithm,
            lambda_angular,
            eta_angular
        )

    else:
        raise NotImplementedError('Cannot generate symmetry functions for '
                                  + '`sftype`s other than 2 or 3.')

    return parent_symfunset
