"""Implementation of functions for generating radial symmetry functions."""

from typing import Optional, Dict, List

import numpy as np

from ase.atoms import Atoms
from ase.units import Bohr

from runnerase.utils.atoms import (get_elements, get_minimum_distances,
                                   get_element_groups)

from .symmetryfunctions import SymmetryFunctionSet, SymmetryFunction


# pylint: disable=too-many-arguments
# Reason: symmetry function have many arguments.
def generate_symmetryfunctions_sftype2(
    parent_symfunset: SymmetryFunctionSet,
    dataset: List[Atoms],
    cutoff: float = 10.0,
    amount: int = 6,
    algorithm: str = 'half',
    elements: Optional[List[str]] = None,
    min_distances: Optional[Dict[str, float]] = None,
) -> None:
    """Based on a dataset, generate a set of type 2 symmetry functions."""
    # If no elements were provided use all elements of this set.
    if elements is None:
        elements = get_elements(dataset)

    # For radial symmetry functions, minimum element distances are required.
    if min_distances is None:
        min_distances_ang = get_minimum_distances(dataset, elements)
        min_distances = {}
        for element, dist in min_distances_ang.items():
            min_distances[element] = dist / Bohr

    # Create one set of symmetry functions for each element pair.
    for element_group in get_element_groups(elements, 2):
        # Get label and save the min_distances for this element pair.
        label = '-'.join(element_group)

        min_dist = 0.1
        if label in min_distances:
            min_dist = min_distances[label]
        rmin = {label: min_dist}

        # Add `amount` symmetry functions to a fresh symmetry function set.
        element_symfunset = SymmetryFunctionSet(min_distances=rmin)
        for _ in range(amount):
            element_symfunset += SymmetryFunction(sftype=2, cutoff=cutoff,
                                                  elements=element_group)

        # Set the symmetry function coefficients. This modifies symfunset.
        set_radial_coefficients(element_symfunset, cutoff,
                                algorithm=algorithm,
                                elements=element_group)

        parent_symfunset.append(element_symfunset)


def set_radial_coefficients(
    sfset: SymmetryFunctionSet,
    cutoff: float,
    algorithm: str,
    elements: List[str]
) -> None:
    """Calculate the coefficients of radial symmetry functions."""
    if sfset.min_distances is not None:
        rmin = sfset.min_distances['-'.join(elements)]
    else:
        rmin = 1.0

    dturn: float = 0.5 * cutoff - rmin
    interval: float = dturn / float(len(sfset) - 1.0)

    interval_shift: float = (cutoff - rmin) / len(sfset)

    for idx, symfun in enumerate(sfset.symmetryfunctions):
        rturn: float = 0.5 * cutoff - interval * float(idx)

        # Equally spaced at G(r) = 0.5.
        if algorithm == 'half':
            symfun.coefficients = calc_coeff_half(rturn, cutoff)

        # Equally spaced turning points.
        elif algorithm == 'turn':
            symfun.coefficients = calc_coeff_turn(rturn, cutoff)

        elif algorithm == 'shifted':
            symfun.coefficients = calc_coeff_shifted(
                rmin + interval_shift * float(idx)
            )

        else:
            raise NotImplementedError(f"Unknown algorithm '{algorithm}'.")


def calc_coeff_shifted(rturn: float) -> List[float]:
    """Return a radial symmetry function with maximum at `rturn`.

    The eta value 0.9 works well but does not have a physical motivation.
    Users are encouraged to play around!
    """
    return [0.9, rturn]


def calc_coeff_turn(
    rturn: float,
    cutoff: float
) -> List[float]:
    """Calculate coefficients of one radial symfun with turnpoint `rturn`."""
    phi = np.pi * rturn / cutoff
    cosphi: float = np.cos(phi)
    sinphi: float = np.sin(phi)

    df1 = 2.0 * (cosphi + 1.0)
    df2 = 8.0 * df1 * rturn**2
    df3 = 2.0 * df1 - 4.0 * phi * sinphi
    sqrtterm: float = np.sqrt(df3**2 + df2 * np.pi**2 / cutoff**2 * cosphi)
    eta = (df3 + sqrtterm) / df2

    return [eta, 0.0]


def calc_coeff_half(
    rturn: float,
    cutoff: float
) -> List[float]:
    """Calculate coefficients of one radial symfun where f(`rturn`) = 0.5."""
    phi = np.pi * rturn / cutoff
    cosphi: float = np.cos(phi)
    logphi: float = np.log(cosphi + 1.0)
    eta = logphi / rturn**2

    return [eta, 0.0]
