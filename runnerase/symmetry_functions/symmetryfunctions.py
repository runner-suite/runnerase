"""Implementation of classes for storing symmetry functions.

Attributes
----------
SymmetryFunction:
    Storage container for a single symmetry function.
SymmetryFunctionSet:
    Storage container for a collection of symmetry functions.
"""

from typing import Optional, Dict, List, Union, Tuple, Iterator

from ase.calculators.calculator import CalculatorSetupError

from runnerase.plot import SymmetryFunctionSetPlots


# Custom type specification for lists of symmetry function parameters. Can be
# two kinds of tuples, depending on whether it is a radial or an angular
# symmetry function.
SFListType = Union[Tuple[str, int, str, float, float, float],
                   Tuple[str, int, str, str, float, float, float, float]]


class SymmetryFunction:
    """Generic class for one single symmetry function."""

    # Symmetry functions have a few arguments which need to be given upon
    # class initialization.
    # pylint: disable=too-many-arguments
    def __init__(
        self,
        sftype: Optional[int] = None,
        cutoff: Optional[float] = None,
        elements: Optional[List[str]] = None,
        coefficients: Optional[List[float]] = None,
        sflist: Optional[SFListType] = None
    ) -> None:
        """Initialize the class.

        Parameters
        ----------
        sftype:
            The type of symmetry function.
        cutoff:
            The symmetry function cutoff radius in units of Bohr.
        elements:
            Symbols of the elements described by this symmetry function.
            The first entry of the list gives the central atom, while all
            following entries stand for neighbor atoms. Usually, the number of
            neighbors should be 1 (= radial symfun) or 2 (= angular symfun)
        coefficients:
            The coefficients of this symmetry function. The number of necessary
            coefficients depends on the `sftype` as documented in the RuNNer
            manual.
        sflist:
            A symmetry function in list form. When supplied, the list
            will be parsed into an object of this class.
        """
        self.sftype = sftype
        self.cutoff = cutoff
        self.elements = elements
        self.coefficients = coefficients

        if sflist is not None:
            self.from_list(sflist)

    def __repr__(self) -> str:
        """Show a clean summary of the class object and its contents."""
        return f'{self.__class__.__name__}(sftype={self.sftype}, ' \
               + f'cutoff={self.cutoff}, ' \
               + f'elements={self.elements}, coefficients={self.coefficients})'

    def __add__(
        self,
        blob: Union['SymmetryFunctionSet', 'SymmetryFunction']
    ) -> Union['SymmetryFunctionSet', 'SymmetryFunction']:
        """Define addition behaviour.

        `SymmetryFunction`s can be treated like mathematical objects. A SF may
        be added to another SF which will return a `SymmetryFunctionSet`. A SF
        may also be added to an existing `SymmetryFunctionSet`.

        Parameters
        ----------
        blob:
            The object to which this object will be added.

        Returns
        -------
        blob/sfset:
            If a `SymmetryFunctionSet` was supplied as the input argument
            `blob`, it will be returned with this object added to its storage.
            Otherwise, a new `sfset` will be created and returned.
        """
        if isinstance(blob, SymmetryFunctionSet):
            blob += self
            return blob

        if isinstance(blob, SymmetryFunction):
            sfset = SymmetryFunctionSet()
            sfset += self
            sfset += blob
            return sfset

        raise TypeError('unsupported operand type(s) for multiplication.')

    def __mul__(self, multiplier: int) -> 'SymmetryFunctionSet':
        """Define multiplication behaviour.

        `SymmetryFunction`s can be treated like mathematical objects. A SF may
        be multiplied by an integer to replicate it.

        Parameters
        ----------
        multiplier:
            The number of times this object will be replicated.

        Returns
        -------
        sfset:
            A set of symmetry functions containing the replicated object.
        """
        sfset = SymmetryFunctionSet()
        for _ in range(multiplier):
            sfset += self.copy()

        return sfset

    def __rmul__(self, multiplier: int) -> 'SymmetryFunctionSet':
        """Multiply the object from the left."""
        return self.__mul__(multiplier)

    def copy(self) -> 'SymmetryFunction':
        """Make the object copyable."""
        elements = None
        if self.elements is not None:
            elements = self.elements.copy()

        coefficients = None
        if self.coefficients is not None:
            coefficients = self.coefficients.copy()

        return SymmetryFunction(
            self.sftype,
            self.cutoff,
            elements,
            coefficients
        )

    @property
    def tag(self) -> str:
        """Show a human-readable equivalent of the `sftype` parameter."""
        if self.sftype in [2, 22, 23]:
            tag = 'radial'
        elif self.sftype in [3, 4, 5, 25, 26, 27]:
            tag = 'angular'
        else:
            tag = 'unknown'

        return tag

    def to_runner(self, fmt: str = '16.8f') -> str:
        """Convert the symmetry function into RuNNer input.nn format."""
        if self.coefficients is None or self.elements is None:
            raise CalculatorSetupError('Symmetryfunctions not fully defined.')

        centralatom = self.elements[0]
        neighbors = self.elements[1:]
        coefficients = [f'{c:{fmt}}' for c in self.coefficients]

        string = f'{centralatom} {self.sftype} ' \
                 + ' '.join(neighbors) \
                 + ' '.join(coefficients) \
                 + f' {self.cutoff:{fmt}}'

        return string

    def todict(self):
        """Return a dictionary representation of the object."""
        return {
            'sftype': self.sftype,
            'cutoff': self.cutoff,
            'elements': self.elements,
            'coefficients': self.coefficients
        }

    def to_list(self) -> SFListType:
        """Create a list representation of the symmetry function."""
        if (self.elements is None or self.coefficients is None
           or self.sftype is None or self.cutoff is None):
            raise AttributeError('Symmetry function not fully defined.')

        if self.tag == 'radial':
            return (self.elements[0], self.sftype, self.elements[1],
                    self.coefficients[0], self.coefficients[1], self.cutoff)

        if self.tag == 'angular':
            return (self.elements[0], self.sftype, self.elements[1],
                    self.elements[2], self.coefficients[0],
                    self.coefficients[1], self.coefficients[2], self.cutoff)

        raise NotImplementedError('Cannot convert symmetry functions of '
                                  + f'type {self.tag} to list.')

    def from_list(self, sflist: SFListType) -> None:
        """Fill storage from a list of symmetry function parameters."""
        self.sftype = sflist[1]
        self.cutoff = sflist[-1]

        # The type: ignore statements are justified because the len() checks in
        # the if-statements make sure that the number of parameters is
        # compatible with the sftype.
        if self.tag == 'radial' and len(sflist) == 6:
            self.elements = [sflist[0], sflist[2]]
            self.coefficients = [float(sflist[3]), float(sflist[4])]

        elif self.tag == 'angular' and len(sflist) == 8:
            self.elements = [sflist[0], sflist[2], sflist[3]]  # type: ignore
            self.coefficients = [float(sflist[4]), float(sflist[5]),
                                 float(sflist[6])]  # type: ignore
        else:
            raise ValueError('sftype incompatible with number of parameters.')


class SymmetryFunctionSet:
    """Class for storing groups/sets of symmetry functions."""

    def __init__(
        self,
        sflist: Optional[List[SFListType]] = None,
        min_distances: Optional[Dict[str, float]] = None
    ) -> None:
        """Initialize the class.

        This class can be nested to group symmetry functions together.

        Parameters
        ----------
        sflist:
            A list of symmetry functions in list format.
        min_distances:
            The minimum distances for all element pairs in the dataset.
        """
        self._sets: List[SymmetryFunctionSet] = []
        self._symmetryfunctions: List[SymmetryFunction] = []
        self.min_distances = min_distances

        if sflist is not None:
            self.from_list(sflist)

    def __str__(self) -> str:
        """Show a clean summary of the class object and its contents."""
        n_sets = len(self._sets)
        n_symmetryfunctions = len(self._symmetryfunctions)

        return f'{self.__class__.__name__}(type={self.sftypes}, ' \
               + f'sets={n_sets}, symmetryfunctions={n_symmetryfunctions})'

    def __repr__(self) -> str:
        """Show a unique summary of the class object."""
        return f'{self.to_list()}'

    def __len__(self) -> int:
        """Return the number of symmetry functions as the object length."""
        return len(self.storage)

    def __add__(
        self,
        blob: Union['SymmetryFunctionSet', SymmetryFunction]
    ) -> 'SymmetryFunctionSet':
        """Overload magic routine to enable nesting of multiple sets."""
        # Add a new subset of symmetry functions.
        if isinstance(blob, SymmetryFunctionSet):
            self._sets += blob.sets
            self._symmetryfunctions += blob.symmetryfunctions

        # Add a single symmetry function to storage.
        elif isinstance(blob, SymmetryFunction):
            self._symmetryfunctions.append(blob)
        else:
            raise NotImplementedError

        return self

    def __iter__(self) -> Iterator[SymmetryFunction]:
        """Iterate over all stored symmetry functions."""
        for symmetryfunction in self.storage:
            yield symmetryfunction

    def todict(self):
        """Return a dictionary representation of the object."""
        return {
            'sets': self._sets,
            'symmetryfunctions': self._symmetryfunctions,
            'min_distances': self.min_distances
        }

    def to_list(self) -> List[SFListType]:
        """Create a list of all stored symmetryfunctions."""
        symmetryfunction_list = []
        for symmetryfunction in self.storage:
            symmetryfunction_list.append(symmetryfunction.to_list())
        return symmetryfunction_list

    def from_list(
        self,
        symmetryfunction_list: List[SFListType]
    ) -> None:
        """Fill storage from a list of symmetry functions."""
        for entry in symmetryfunction_list:
            self.append(SymmetryFunction(sflist=entry))

    @property
    def sets(self) -> List['SymmetryFunctionSet']:
        """Show a list of all stored `SymmetryFunctionSet` objects."""
        return self._sets

    @property
    def symmetryfunctions(self) -> List[SymmetryFunction]:
        """Show a list of all stored `SymmetryFunction` objects."""
        return self._symmetryfunctions

    @property
    def storage(self) -> List[SymmetryFunction]:
        """Show all stored symmetry functions recursively."""
        storage = self.symmetryfunctions.copy()
        for sfset in self.sets:
            storage += sfset.storage

        return storage

    @property
    def sftypes(self) -> Optional[str]:
        """Show a list of symmetry function types in self.symmetryfunctions."""
        sftypes = list(set(sf.tag for sf in self.symmetryfunctions))
        if len(sftypes) == 1:
            return sftypes[0]

        if len(sftypes) > 1:
            return 'mixed'

        return None

    @property
    def elements(self) -> Optional[List[str]]:
        """Show a list of all elements covered in self.symmetryfunctions."""
        # Store all elements of all symmetryfunctions.
        elements = []

        for sfset in self.sets:
            if sfset.elements is not None:
                elements += sfset.elements

        for symfun in self.symmetryfunctions:
            if symfun.elements is not None:
                elements += symfun.elements

        # Remove duplicates.
        elements = list(set(elements))

        # If the list is empty, return None instead.
        if len(elements) == 0:
            return None

        return elements

    @property
    def cutoffs(self) -> Optional[List[Optional[float]]]:
        """Show a list of all cutoffs in self.symmetryfunctions."""
        # Collect the cutoff values of all symmetryfunctions.
        cutoffs = list(set(sf.cutoff for sf in self.storage))

        # If the list is empty, return None instead.
        if len(cutoffs) == 0:
            return None

        return cutoffs

    def append(
        self,
        blob: Union['SymmetryFunctionSet', SymmetryFunction]
    ) -> None:
        """Append a data `blob` to the internal storage."""
        if isinstance(blob, SymmetryFunctionSet):
            self._sets.append(blob)

        elif isinstance(blob, SymmetryFunction):
            self._symmetryfunctions.append(blob)

        else:
            raise CalculatorSetupError(
                f'{self.__class__.__name__} can only store data of'
                + 'type SymmetryFunctionSet or SymmetryFunction.'
            )

    def reset(self) -> None:
        """Clear all symmetryfunctions and sets from storage."""
        self._sets: List[SymmetryFunctionSet] = []  # type: ignore
        self._symmetryfunctions: List[SymmetryFunction] = []  # type: ignore

    @property
    def plot(self) -> SymmetryFunctionSetPlots:
        """Create a plotting interface."""
        return SymmetryFunctionSetPlots(self.storage)
