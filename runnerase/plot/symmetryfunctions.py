#!/usr/bin/env python3
"""Plot symmetry functions."""

from typing import Optional, Tuple

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from .setup import GenericPlots


def _calc_cutoff_cosine(symfun, xrange):
    return 0.5 * (np.cos(np.pi * xrange / symfun.cutoff) + 1)


def _calc_cutoff_tanh(symfun, xrange):
    return np.tanh(1 - xrange / symfun.cutoff)**3


def _calc_radial_type2(symfun, xrange, apply_cutoff=False):
    if symfun.sftype != 2:
        raise RuntimeError('Wrong type of symmetry function. Only type 2!')
    coeff1, coeff2 = symfun.coefficients

    cutoff = 1.0
    if apply_cutoff:
        cutoff = _calc_cutoff_cosine(symfun, xrange)

    return np.exp(-coeff1 * (xrange - coeff2)**2) * cutoff


def _calc_angular_type3(symfun, xrange, apply_cutoff=False):
    if symfun.sftype != 3:
        raise RuntimeError('Wrong type of symmetry function. Only type 3!')
    _, coeff2, coeff3 = symfun.coefficients

    cutoff = 1.0
    if apply_cutoff:
        cutoff = _calc_cutoff_cosine(symfun, xrange)

    return 2**(1 - coeff3) * (1 + coeff2 * np.cos(xrange))**coeff3 * cutoff


class SymmetryFunctionSetPlots(GenericPlots):
    """A plotting interface for a set of symmetry functions."""

    def __init__(
        self,
        symmetryfunctions  # type: ignore
    ) -> None:
        """Initialize the class."""
        self.symmetryfunctions = symmetryfunctions

    # The plots need to be flexible, so we allow multiple kwarg options.
    # pylint: disable=too-many-branches, too-many-arguments, too-many-locals
    def radial(
        self,
        axes: Optional[plt.Axes] = None,
        figsize: Optional[Tuple[int, int]] = None,
        num_columns: int = 2,
        show_legend: bool = False,
        cutoff_function: bool = False
    ) -> plt.Axes:
        """Create lineplots of radial symmetry functions.

        Parameters
        ----------
        axes:
            A maplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        figsize:
            The x and y size of the figure.
        num_columns:
            How many plots will show up in one row.
        show_legend:
            Whether a legend will be shown in each subplot or not.
        cutoff_function:
            Whether to include the cutoff function in the calculation.
        """
        # Get a list of all element groups.
        elements = []
        for symfun in self.symmetryfunctions:
            if symfun.sftype != 2:
                continue

            if symfun.elements not in elements:
                elements.append(symfun.elements)

        # Determine the maximum number of axes rows and columns we need.
        num_triplets = len(elements)
        max_rows = int(np.ceil(num_triplets / num_columns))

        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            if len(elements) == 1:
                axes = plt.gca()
            else:
                # Create subplots for each group of elements.
                fig, axes = plt.subplots(max_rows, num_columns,
                                         figsize=figsize)

                # Delete those axes we do not need due to the # of triplets.
                for ax in axes.flatten()[num_triplets:]:  # type: ignore
                    fig.delaxes(ax)

        # Choose the right style for a bar plot.
        self.add_style('line')

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            for idx, element_group in enumerate(elements):

                if isinstance(axes, matplotlib.axes.Axes):
                    axis = axes
                else:
                    axis = axes.flatten()[idx]

                for symfun in self.symmetryfunctions:

                    xrange = np.linspace(0.0, symfun.cutoff, 100)

                    # Skip all symmetry functions which do not belong to the
                    # element_group.
                    if symfun.elements != element_group:
                        continue

                    # Plot only radial symmetry functions of type 2.
                    if symfun.sftype != 2:
                        continue

                    # Calculate the function values w/ or w/o cutoff function.
                    sfvalues = _calc_radial_type2(
                        symfun,
                        xrange,
                        apply_cutoff=cutoff_function
                    )

                    # Generate the label.
                    label = r'$\eta = $' + f'{symfun.coefficients[0]:.3f}' \
                            + r' $a_0^{-2}$,' \
                            + r' $R_\mathrm{s} = $' \
                            + f'{symfun.coefficients[1]:.3f}'

                    axis.plot(xrange, sfvalues, '-', label=label)

                    # Set title and labels.
                    # For multiple plots, set subplot title.
                    if not isinstance(axes, matplotlib.axes.Axes):
                        axis.set_title(f'Elements {element_group}')

                    axis.set_xlabel('Pairwise Distance $r$ / $a_0$')
                    axis.set_ylabel('Symmetry Function Value')
                    axis.set_ylim(0.0, 1.1)
                    axis.set_xlim(0.0, symfun.cutoff + 1)

                    if show_legend is True:
                        axis.legend()

            # Set super title.
            plt.suptitle('Radial Symmetry Functions')

        return axes

    # pylint: disable=too-many-locals
    def angular(
        self,
        axes: Optional[plt.Axes] = None,
        figsize: Optional[Tuple[int, int]] = None,
        num_columns: int = 3,
        show_legend: bool = False
    ) -> plt.Axes:
        """Create lineplots of angular symmetry functions.

        Parameters
        ----------
        axes:
            A maplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        figsize:
            The x and y size of the figure.
        num_columns:
            How many plots will show up in one row.
        show_legend:
            Whether to show a legend for each subplot.
        """
        # Get a list of all element groups.
        elements = []
        for symfun in self.symmetryfunctions:
            if symfun.sftype != 3:
                continue

            if symfun.elements not in elements:
                elements.append(symfun.elements)

        # Determine the maximum number of axes rows and columns we need.
        num_triplets = len(elements)
        max_rows = int(np.ceil(num_triplets / num_columns))

        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            if len(elements) == 1:
                axes = plt.gca()
            else:
                # Create subplots for each group of elements.
                fig, axes = plt.subplots(max_rows, num_columns,
                                         figsize=figsize)

                # Delete those axes we do not need due to the # of triplets.
                for ax in axes.flatten()[num_triplets:]:  # type: ignore
                    fig.delaxes(ax)

        # Choose the right style for a bar plot.
        self.add_style('line')

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            for idx, element_group in enumerate(elements):

                if isinstance(axes, matplotlib.axes.Axes):
                    axis = axes
                else:
                    axis = axes.flatten()[idx]

                for symfun in self.symmetryfunctions:

                    xrange = np.linspace(0.0, 2 * np.pi, 360)

                    # Skip all symmetry functions which do not belong to the
                    # element_group.
                    if symfun.elements != element_group:
                        continue

                    # Plot only angular symmetry functions of type 3.
                    if symfun.sftype != 3:
                        continue

                    sfvalues = _calc_angular_type3(symfun, xrange)

                    # Generate the label.
                    label = r'$\lambda = $' + f'{symfun.coefficients[1]:.1f},' \
                            + r' $\zeta = $' \
                            + f'{symfun.coefficients[2]:.1f}'

                    axis.plot(xrange, sfvalues, '-', label=label)

                    # Set title and labels.
                    # For multiple plots, set subplot title.
                    if not isinstance(axes, matplotlib.axes.Axes):
                        axis.set_title(f'Elements {element_group}')

                    axis.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi,
                                     2 * np.pi])
                    axis.set_xticklabels(['0', '90', '180', '270', '360'])
                    axis.set_xlabel(r'Angle $\Theta$ / degree')
                    axis.set_ylabel('Symmetry Function Value')

                    if show_legend is True:
                        axis.legend()

            # Set super title.
            plt.suptitle('Angular Symmetry Functions')

        return axes
