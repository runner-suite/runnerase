#!/usr/bin/env python3
# encoding: utf-8
# pylint: disable=too-many-lines
# Reason: disabled until I can think of a better way to split this module.
"""Plot direct properties of the calculator (dataset, energy, forces,...)."""

from typing import Optional, List

import numpy as np
import matplotlib.pyplot as plt

from runnerase.analysis import RunnerAnalysis

from .setup import GenericPlots


# pylint: disable=too-many-public-methods
# Reason: the plotting class provides many different plots.
# Each of them is a public method.
class RunnerPlots(GenericPlots, RunnerAnalysis):
    """A plotting interface for the Runner calculator."""

    def energy_distribution(
        self,
        energy: np.ndarray,
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Create a scatter plot of the energy distribution in `energy`.

        Parameters
        ----------
        energy:
            A list of energies.
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(range(len(energy)), energy)

            # Set title and labels.
            axes.set_xlabel('Structure Index')
            axes.set_ylabel('Energy $E$ / eV atom$^{-1}$')
            axes.set_title('Energy Distribution in the Training Dataset')

        return axes

    def energy_distribution_reference(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the energy distribution in `self.dataset`.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned.
        """
        return self.energy_distribution(self.energy_per_atom_reference,
                                        axes=axes)

    def energy_distribution_hdnnp(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the predicted energy distribution.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned.
        """
        return self.energy_distribution(self.energy_per_atom_hdnnp, axes=axes)

    def totalforce_distribution(
        self,
        forces: np.ndarray,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the total forces of each atom in `dataset`.

        Parameters
        ----------
        forces:
            The atomic forces (num_structures x num_atoms x 3).
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        # Calculate the total force of each atom.
        atomic_forces = np.vstack(forces)  # type: ignore
        totalforce = np.sum(np.square(atomic_forces), axis=1)**0.5

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(range(len(totalforce)), totalforce)

            # Set title and labels.
            axes.set_xlabel('Structure Index')
            axes.set_ylabel(r'Total Force $F$ / eV $\AA^{-1}$')
            axes.set_title('Total Atomic Force Distribution')

        return axes

    def totalforce_distribution_reference(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the total forces of each atom in `dataset`.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned.
        """
        return self.totalforce_distribution(self.forces_reference, axes=axes)

    def totalforce_distribution_hdnnp(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the total force prediction of each atom.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        return self.totalforce_distribution(self.forces_hdnnp, axes=axes)

    def force_distribution(
        self,
        forces: np.ndarray,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the atomic force of each atom in `dataset`.

        Parameters
        ----------
        forces:
            The atomic forces (num_structures x num_atoms x 3).
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        # Calculate the total force of each atom.
        atomic_forces = np.vstack(forces)  # type: ignore

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(range(atomic_forces.shape[0]), atomic_forces[:, 0],
                         label=r'$F_{\mathrm{x}}$')
            axes.scatter(range(atomic_forces.shape[0]), atomic_forces[:, 1],
                         label=r'$F_{\mathrm{y}}$')
            axes.scatter(range(atomic_forces.shape[0]), atomic_forces[:, 2],
                         label=r'$F_{\mathrm{z}}$')

            # Set title and labels.
            axes.set_xlabel('Structure Index')
            axes.set_ylabel(r'Total Force $F$ / eV $\AA^{-1}$')
            axes.set_title('Atomic Force Distribution')

            plt.legend()

        return axes

    def force_distribution_reference(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the forces of each atom in `dataset`.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        return self.force_distribution(self.forces_reference, axes=axes)

    def force_distribution_hdnnp(
        self,
        axes: Optional[plt.Axes] = None
    ) -> plt.Axes:
        """Create a scatter plot of the force prediction of each atom.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        return self.force_distribution(self.forces_hdnnp, axes=axes)

    def energy_deviation_vs_reference(
        self,
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Create a scatter plot of energy deviation vs. reference energy.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        # Calculate the deviation in meV.
        energy_hdnnp = self.energy_per_atom_hdnnp
        energy_reference = self.energy_per_atom_reference
        deviation = (energy_hdnnp - energy_reference) * 1000

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(energy_reference, deviation)

            # Set title and labels.
            axes.set_xlabel('Reference Energy $E$ / eV atom$^{-1}$')
            axes.set_ylabel(r'Energy Deviation $\Delta E$ / meV atom$^{-1}$')
            axes.set_title('Energy Deviation')

        return axes

    def energy_hdnnp_vs_reference(
        self,
    ) -> plt.Figure:
        """Create a scatter plot of predicted energy vs. reference energy."""
        # Create a new figure for the histogram, if not given.
        fig = plt.figure()

        energy_hdnnp = self.energy_per_atom_hdnnp
        energy_reference = self.energy_per_atom_reference

        # Determine the axis limits.
        min_ax_lim: float = min(energy_hdnnp.min(), energy_reference.min())
        max_ax_lim: float = max(energy_hdnnp.max(), energy_reference.max())

        # Construct the axes for the three different plots.
        gridspec = fig.add_gridspec(2, 2,  width_ratios=(7, 2),
                                    height_ratios=(2, 7), left=0.1,
                                    right=0.9, bottom=0.1, top=0.9,
                                    wspace=0.1, hspace=0.2)

        ax_scatter = fig.add_subplot(gridspec[1, 0])
        ax_histx = fig.add_subplot(gridspec[0, 0], sharex=ax_scatter)
        ax_histy = fig.add_subplot(gridspec[1, 1], sharey=ax_scatter)

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create scatter plot with diagonal line.
            ax_scatter.scatter(energy_reference, energy_hdnnp)
            ax_scatter.plot(energy_reference, energy_reference, color='grey')

            # Create histograms.
            ax_histx.hist(energy_reference)
            ax_histy.hist(energy_hdnnp, orientation='horizontal')
            ax_histx.tick_params(labelbottom=False)
            ax_histy.tick_params(labelleft=False)

            # Set axis limits.
            ax_scatter.set_xlim(min_ax_lim, max_ax_lim)
            ax_scatter.set_ylim(min_ax_lim, max_ax_lim)
            ax_histx.set_xlim(min_ax_lim, max_ax_lim)
            ax_histy.set_ylim(min_ax_lim, max_ax_lim)

            # Set title and labels.
            ax_scatter.set_xlabel('Reference Energy $E$ / eV atom$^{-1}$')
            ax_scatter.set_ylabel(r'Predicted Energy $E$ / eV atom$^{-1}$')
            fig.suptitle('Energy Comparison')

        return fig

    def force_deviation_vs_reference(
        self,
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Create a scatter plot of force deviation vs. reference force.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        force_hdnnp = np.vstack(self.forces_hdnnp)  # type: ignore
        force_reference = np.vstack(self.forces_reference)  # type: ignore

        # Calculate the deviation in meV.
        deviation = abs(force_hdnnp - force_reference) * 1000

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(force_reference[:, 0], deviation[:, 0],
                         label=r'$F_{\mathrm{x}}$')
            axes.scatter(force_reference[:, 1], deviation[:, 1],
                         label=r'$F_{\mathrm{y}}$')
            axes.scatter(force_reference[:, 2], deviation[:, 2],
                         label=r'$F_{\mathrm{z}}$')

            # Set title and labels.
            axes.set_xlabel(r'Reference Forces $F$ / eV $\AA^{-1}$')
            axes.set_ylabel(r'Force Deviation $\Delta F$ / meV $\AA^{-1}$')
            axes.set_title('Force Deviation')

            axes.legend()

        return axes

    def force_hdnnp_vs_reference(
        self,
    ) -> plt.Figure:
        """Create a scatter plot of predicted forces vs. reference forces."""
        # Create a new figure for the histogram, if not given.
        fig = plt.figure()

        forces_hdnnp = self.forces_hdnnp
        forces_reference = self.forces_reference

        # Create atomic arrays.
        atomic_forces_hdnnp = np.vstack(forces_hdnnp)  # type: ignore
        atomic_forces_reference = np.vstack(forces_reference)  # type: ignore

        # Determine the axis limits.
        min_ax_lim = min(atomic_forces_hdnnp.min(),
                         atomic_forces_reference.min())
        max_ax_lim = max(atomic_forces_hdnnp.max(),
                         atomic_forces_reference.max())

        # Construct the axes for the three different plots.
        gridspec = fig.add_gridspec(2, 2,  width_ratios=(7, 2),
                                    height_ratios=(2, 7), left=0.1,
                                    right=0.9, bottom=0.1, top=0.9,
                                    wspace=0.1, hspace=0.2)

        ax_scatter = fig.add_subplot(gridspec[1, 0])
        ax_histx = fig.add_subplot(gridspec[0, 0], sharex=ax_scatter)
        ax_histy = fig.add_subplot(gridspec[1, 1], sharey=ax_scatter)

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create scatter plot with diagonal line.
            ax_scatter.scatter(atomic_forces_reference[:, 0],
                               atomic_forces_hdnnp[:, 0],
                               label=r'$F_{\mathrm{x}}$')
            ax_scatter.scatter(atomic_forces_reference[:, 1],
                               atomic_forces_hdnnp[:, 1],
                               label=r'$F_{\mathrm{y}}$')
            ax_scatter.scatter(atomic_forces_reference[:, 2],
                               atomic_forces_hdnnp[:, 2],
                               label=r'$F_{\mathrm{z}}$')
            ax_scatter.plot(atomic_forces_reference.flatten(),
                            atomic_forces_reference.flatten(), color='grey')

            # Create histograms.
            ax_histx.hist(atomic_forces_reference[:, 0])
            ax_histy.hist(atomic_forces_hdnnp[:, 0], orientation='horizontal')
            ax_histx.hist(atomic_forces_reference[:, 1])
            ax_histy.hist(atomic_forces_hdnnp[:, 1], orientation='horizontal')
            ax_histx.hist(atomic_forces_reference[:, 2])
            ax_histy.hist(atomic_forces_hdnnp[:, 2], orientation='horizontal')
            ax_histx.tick_params(labelbottom=False)
            ax_histy.tick_params(labelleft=False)

            # Set axis limits.
            ax_scatter.set_xlim(min_ax_lim, max_ax_lim)
            ax_scatter.set_ylim(min_ax_lim, max_ax_lim)
            ax_histx.set_xlim(min_ax_lim, max_ax_lim)
            ax_histy.set_ylim(min_ax_lim, max_ax_lim)

            # Set title and labels.
            ax_scatter.set_xlabel(r'Reference Forces $F$ / eV $\AA^{-1}$')
            ax_scatter.set_ylabel(r'Predicted Forces $F$ / eV $\AA^{-1}$')
            fig.suptitle('Force Comparison')

            ax_scatter.legend()

        return fig

    def comparison_energy_mean_vs_reference(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare energy predictions of multiple neural networks.

        Plot the mean and std dev for all predicted points for an array
        of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.energy_absolute_mean(calcs)

        # Increase size of errorbars if they are smaller than
        # 1e-5 meV/atom on average.
        stddev_corr = ''
        if np.mean(stds) <= 10:
            stds *= 10
            stddev_corr = '\n' + r'$\mu$ scaled by a factor of 10 for clarity'

        energy = self.energy_per_atom_reference

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                energy,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                energy,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            axes.plot(
                energy,
                energy,
                color='grey',
                zorder=0,
                label='Perfect comparison'
            )

            # Set title and labels.
            axes.set_xlabel(r'Reference Energy $E(\mathrm{ref})$ / eV / atom')
            axes.set_ylabel(r'Predicted Energy $E$ / eV / atom')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs'
                           + stddev_corr)

            axes.legend()

        return axes

    def comparison_energy_mean_vs_prediction(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare energy predictions of multiple neural networks.

        Plot the mean and std dev for all predicted points for an array
        of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.energy_absolute_mean(calcs)

        # Increase size of errorbars if they are smaller than
        # 1e-5 meV/atom on average.
        stddev_corr = ''
        if np.mean(stds) <= 1e-4:
            stds *= 10
            stddev_corr = '\n' + r'$\mu$ scaled by a factor of 10 for clarity'

        energy = self.energy_per_atom_hdnnp

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                energy,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                energy,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            axes.plot(
                energy,
                energy,
                color='grey',
                zorder=0,
                label='Perfect comparison'
            )

            # Set title and labels.
            axes.set_xlabel(
                r'Original Prediction $E(\mathrm{HDNNP\;1})$ / eV / atom'
            )
            axes.set_ylabel(r'Predicted Energy $E$ / eV / atom')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs'
                           + stddev_corr)

            axes.legend()

        return axes

    def comparison_energy_error_mean_vs_reference(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare energy errors of multiple neural networks.

        Plot the mean and std dev for the difference between reference and
        prediction for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.energy_error_mean(calcs)

        # Convert to meV/atom.
        means *= 1000
        stds *= 1000

        energy = self.energy_per_atom_reference

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                energy,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                energy,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=2
            )

            # Set title and labels.
            axes.set_xlabel(r'Reference Energy $E(\mathrm{ref})$ / eV / atom')
            axes.set_ylabel(r'Energy Error $E(\mathrm{HDNNP})$ '
                            r'- $E(\mathrm{ref})$ / meV / atom')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_energy_error_mean_vs_prediction(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare energy errors of multiple neural networks.

        Plot the mean and std dev for the difference between reference and
        prediction for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.energy_error_mean(calcs)

        # Convert to meV/atom.
        means *= 1000
        stds *= 1000

        energy = self.energy_per_atom_hdnnp

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                energy,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                energy,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            # Set title and labels.
            axes.set_xlabel(
                r'Original Prediction $E(\mathrm{HDNNP\;1})$ / eV / atom'
            )
            axes.set_ylabel(r'Energy Error $E(\mathrm{HDNNP})$ '
                            r'- $E(\mathrm{ref})$ / meV / atom')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_energy_two_hdnns(
        self,
        calc: RunnerAnalysis,
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare absolute energies of two neural networks.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        energy1 = self.energy_per_atom_hdnnp
        energy2 = calc.energy_per_atom_hdnnp
        energy_ref = self.energy_per_atom_reference

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.plot(
                energy_ref,
                energy_ref,
                color='grey',
                zorder=1,
                label='Reference'
            )
            axes.scatter(energy1, energy2, marker='o', s=3, zorder=2)

            # Set title and labels.
            axes.set_xlabel(r'Energy $E(\mathrm{HDNNP\;1})$ / eV / atom')
            axes.set_ylabel(r'Energy $E(\mathrm{HDNNP\;2})$ / eV / atom')
            axes.set_title('Energy Comparison between two HDNNPs')

            axes.legend()

        return axes

    def comparison_forces_error_mean_vs_reference(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare force errors of multiple neural networks.

        Plot the mean and std dev for the difference between reference and
        prediction for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.forces_error_mean(calcs)

        # Convert to meV/Ang.
        means *= 1000
        stds *= 1000

        forces = self.forces_reference.flatten()

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                forces,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                forces,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            # Set title and labels.
            axes.set_xlabel(r'Reference Forces $F(\mathrm{ref})$ / eV / $\AA$')
            axes.set_ylabel(r'Force Component Error $F(\mathrm{HDNNP})$ '
                            r'- $F(\mathrm{ref})$ / meV / $\AA$')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_forces_error_mean_vs_prediction(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare force errors of multiple neural networks.

        Plot the mean and std dev for the difference between reference and
        prediction for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.forces_error_mean(calcs)

        # Convert to meV/Ang.
        means *= 1000
        stds *= 1000

        forces = self.forces_hdnnp.flatten()

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                forces,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                forces,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            # Set title and labels.
            axes.set_xlabel(
                r'Original Prediction $F(\mathrm{HDNNP\;1})$ / eV / $\AA$'
            )
            axes.set_ylabel(r'Force Component Error $F(\mathrm{HDNNP})$ '
                            r'- $F(\mathrm{ref})$ / meV / $\AA$')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_forces_mean_vs_reference(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare force components of multiple neural networks.

        Plot the mean and std dev for the force predictions
        for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.forces_absolute_mean(calcs)

        forces = self.forces_reference.flatten()

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                forces,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                forces,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            axes.plot(
                forces,
                forces,
                color='grey',
                zorder=0,
                label='Perfect comparison'
            )

            # Set title and labels.
            axes.set_xlabel(r'Reference Forces $F$ / eV / $\AA$')
            axes.set_ylabel(r'Predicted Forces $F$ / eV / $\AA$')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_forces_mean_vs_prediction(
        self,
        calcs: List[RunnerAnalysis],
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare force components of multiple neural networks.

        Plot the mean and std dev for the force predictions
        for an array of multiple trained HDNNPs.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        means, stds = self.forces_absolute_mean(calcs)

        forces = self.forces_hdnnp.flatten()

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.scatter(
                forces,
                means,
                marker='o',
                s=3,
                label=r'Mean $\sigma$',
                zorder=2
            )

            axes.errorbar(
                forces,
                means,
                stds,
                linestyle='None',
                marker='',
                capsize=3,
                capthick=1,
                ecolor='grey',
                elinewidth=1,
                label=r'Std. Dev. $\mu$ ',
                alpha=0.5,
                zorder=1
            )

            axes.plot(
                forces,
                forces,
                color='grey',
                zorder=0,
                label='Perfect comparison'
            )

            # Set title and labels.
            axes.set_xlabel(
                r'Original Prediction $F(\mathrm{HDNNP\;1})$ / eV / $\AA$'
            )
            axes.set_ylabel(r'Predicted Forces $F$ / eV / $\AA$')
            axes.set_title(f'Mean / Std. Dev. for {len(calcs) + 1} HDNNPs')

            axes.legend()

        return axes

    def comparison_forces_two_hdnns(
        self,
        calc: RunnerAnalysis,
        axes: Optional[plt.Axes] = None,
    ) -> plt.Axes:
        """Compare forces of two neural networks.

        Parameters
        ----------
        axes:
            A matplotlib.pyplot `Axes` instance to which the data will be added.
            If no axis is supplied, a new one is generated and returned instead.
        """
        # If no axis object was provided, use the last one or create a new one.
        if axes is None:
            axes = plt.gca()

        forces1 = self.forces_hdnnp
        forces2 = calc.forces_hdnnp
        forces_ref = self.forces_reference.flatten().astype(np.float64)

        # Use a context manager to apply styles locally.
        with plt.style.context(self.styles):

            # Create plot.
            axes.plot(
                forces_ref,
                forces_ref,
                color='grey',
                zorder=1,
                label='Reference'
            )
            axes.scatter(forces1, forces2, marker='o', s=3, zorder=2)

            # Set title and labels.
            axes.set_xlabel(r'Forces $F(\mathrm{HDNNP\;1})$ / eV / $\AA$')
            axes.set_ylabel(r'Forces $F(\mathrm{HDNNP\;2})$ / eV / $\AA$')
            axes.set_title('Force Component Comparison between two HDNNPs')

            axes.legend()

        return axes
