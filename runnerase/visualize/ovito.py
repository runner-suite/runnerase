#!/usr/bin/env python3
# encoding: utf-8
"""ASE - OVITO interface routines."""

from typing import List, Optional, Union, Tuple

import os

from ase.atoms import Atoms
from ase.io import write

try:
    from ovito.io import import_file
    from ovito.io.ase import ase_to_ovito
    from ovito.vis import Viewport
    from ovito.modifiers import ColorCodingModifier
    from ovito.pipeline import StaticSource, Pipeline
    from ovito.qt_compat import QtCore
    from ovito.vis import ColorLegendOverlay
except ImportError as err:
    raise ImportError(
        'Ovito module is not installed. Please install it using pip.'
    ) from err


# Filename of the temporary trajectory file created for generating a pipeline
# from a list of Atoms objects.
TEMP_TRAJ_FILENAME = '.ovito_traj_temp.xyz'


def generate_pipeline(atoms: Union[Atoms, List[Atoms]]):
    """Create an Ovito pipeline from a single atoms or a list of atoms."""
    if not isinstance(atoms, list):
        pipeline = Pipeline(
            source=StaticSource(
                data=ase_to_ovito(atoms, data_collection=None)
            )
        )
    else:

        columns = None
        ovito_columns = [
            'Particle Type',
            'Position.X',
            'Position.Y',
            'Position.Z'
        ]
        if 'color' in atoms[0].arrays.keys():
            columns = ['symbols', 'positions', 'color']
            ovito_columns.append('color')

        write(TEMP_TRAJ_FILENAME, atoms, columns=columns)
        pipeline = import_file(
            TEMP_TRAJ_FILENAME,
            columns=ovito_columns
        )

        pipeline.add_to_scene()

    return pipeline


# pylint: disable=too-many-arguments
# Reason: this wrapper function is supposed to allow the user many ways to
# change its behaviour to be generally applicable. The arguments are well
# documented, they stay.
def render(
    atoms_in: Union[Atoms, List[Atoms]],
    filename: str,
    renderer=None,
    viewport: Viewport = Viewport(),
    cvalues: Optional[Union[List[float], List[List[float]]]] = None,
    cmin: float = -1.,
    cmax: float = 1.,
    ctitle: str = 'Colorbar',
    image_size: Tuple[int, int] = (1000, 1000),
    fps: int = 60
):
    """Render `atoms_in` using Ovito.

    Parameters
    ----------
    atoms_in:
        The structure to be rendered.
    filename:
        The output filename for the rendering.
    renderer:
        The rendering engine to be used.
    viewport:
        The viewport defines the position, angle,
        and zoom of the camera when rendering.
    cvalues:
        A list of values which are used for coloring
        the atoms in the rendering.
    cmin:
        Minimum value of the colorbar when cvalues is given.
    cmax:
        Maximum value of the colorbar when cvalues is given.
    ctitle:
        Colorbar title.
    image_size:
        Output size of the image.
    fps:
        Frame per second when rendering an animation.
    """
    if cvalues is not None:
        if isinstance(atoms_in, list):
            if len(cvalues) != len(atoms_in):
                raise RuntimeError('One entry has to exist in cvalues for '
                                   'every entry in atoms_in.')

            for idx, struc in enumerate(atoms_in):
                # Create an array to store the color coding values.
                struc.arrays['color'] = cvalues[idx]
        else:
            atoms_in.arrays['color'] = cvalues

    pipeline = generate_pipeline(atoms_in)

    # Apply color coding.
    if cvalues is not None:
        mod = ColorCodingModifier(
            property='color',
            start_value=cmin,
            end_value=cmax,
            gradient=ColorCodingModifier.BlueWhiteRed()
        )
        pipeline.modifiers.append(mod)

        algm = QtCore.Qt.AlignTrailing ^ QtCore.Qt.AlignVCenter  # type: ignore
        viewport.overlays.append(
            ColorLegendOverlay(
                modifier=mod,
                alignment=algm,
                orientation=QtCore.Qt.Vertical,  # type: ignore
                offset_x=-0.1,
                title=ctitle
            )
        )

    # Rendering.
    if isinstance(atoms_in, list):
        viewport.render_anim(
            filename=filename,
            size=image_size,
            renderer=renderer,
            fps=fps
        )

        if os.path.exists(TEMP_TRAJ_FILENAME):
            os.remove(TEMP_TRAJ_FILENAME)

    else:
        viewport.render_image(
            size=image_size,
            filename=filename,
            renderer=renderer
        )

    pipeline.remove_from_scene()
