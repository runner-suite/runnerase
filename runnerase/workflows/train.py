#!/usr/bin/env python3
# encoding: utf-8
"""Implementation of a general automatic training workflow."""

from typing import Optional, Any, Dict

import matplotlib.pyplot as plt

from runnerase.symmetry_functions.wrappers import generate_symmetryfunctions
from runnerase.calculators.runner import Runner
from runnerase.utils.dicts import update
from runnerase.utils.defaultoptions import DEFAULT_TRAINING_PARAMETERS


def train_hdnnp(
    calc: Runner,
    user_settings: Optional[Dict[str, Any]] = None
) -> None:
    """Execute all three RuNNer mode consecutively.

    The user can specify custom settings through a dictionary. If no
    settings are provided, sensible defaults are chosen.

    Parameters
    ----------
    calc:
        A Runner calculator containing, at the least, a training dataset,
        and the command for executing RuNNer.
    user_settings:
        A dictionary similar to DEFAULT_TRAINING_PARAMETERS containing user
        defined training parameters.

    """
    settings: Dict[str, Any] = DEFAULT_TRAINING_PARAMETERS.copy()
    if user_settings is not None:
        update(settings, user_settings)

    # Filter the dataset.
    if 'filter' in settings:
        filtered_data = calc.filter_dataset(
            settings['filter']['function'],
            **settings['filter']['kwargs']
        )
    calc.dataset = filtered_data

    # Parametrize symmetry functions based on the filtered data.
    add_symmetry_functions(calc, settings['symfuns'])

    # Execute mode 1.
    run_mode1(calc, settings['mode1'])

    # Run Mode 2.
    if 'sfvalues' in calc.results:
        run_mode2(calc, settings['mode2'])

    # Run mode 3.
    if 'fitresults' in calc.results:
        run_mode3(calc, settings['mode3'])


def add_symmetry_functions(
    calc: Runner,
    params: Dict[str, Any]
) -> None:
    """Generate a set of symmetry functions for the fit based on `dataset`.

    Both radial and angular symmetry functions are generated.

    Parameters
    ----------
    calc:
        A Runner calculator containing a training dataset.
    params:
        A dictionary with user supplied parameters for the generation
        of the symmetry functions. At least the keys in
        `DEFAULT_TRAINING_PARAMETERS['symfuns']` must be present.
    """
    if calc.dataset is None:
        raise ValueError('Cannot generate symmetry functions. '
                         'Dataset on calculator is None.')

    # Reset the symmetry function container.
    calc.parameters['symfunction_short'].reset()

    # Generate radial symmetry functions.
    radials = generate_symmetryfunctions(
        calc.dataset,
        sftype=2,
        algorithm=params['radial']['algorithm'],
        amount=params['radial']['amount'],
        cutoff=params['radial']['cutoff']
    )
    calc.parameters['symfunction_short'] += radials

    # Generate angular symmetry functions.
    angulars = generate_symmetryfunctions(
        calc.dataset,
        sftype=3,
        algorithm=params['angular']['algorithm'],
        amount=params['angular']['amount'],
        cutoff=params['angular']['cutoff'],
        eta_angular=radials
    )
    calc.parameters['symfunction_short'] += angulars


def run_mode1(
    calc: Runner,
    settings: Dict[str, Any]
) -> Runner:
    """Run RuNNer symmetry function value calculation.

    Parameters
    ----------
    calc:
        A Runner calculator containing a training dataset.
    settings:
        A dictionary with user supplied parameters for mode 1.

    Returns
    -------
    calc:
        The updated calculator after the calculation.
    """
    if calc.label is None:
        calc.label = './mode1/mode1'

    # Bugfix for current ASE main: profile.prefix is not updated by label.
    calc.profile.prefix = calc.prefix

    calc.parameters.update(settings)

    calc.run(mode=1)

    # Visualize the symmetry function values.
    if 'sfvalues' in calc.results:
        sfvalues = calc.results['sfvalues']
        plt.subplots(figsize=(14, 6))
        sfvalues.plot.boxplot()
        plt.savefig(f'{calc.directory}/sfvalues.png', dpi=600)
        plt.close()

    return calc


def run_mode2(
    calc: Runner,
    settings: Dict[str, Any]
) -> Runner:
    """Run RuNNer HDNNP training.

    Parameters
    ----------
    calc:
        A Runner calculator that has already run mode 1.
    settings:
        A dictionary with user supplied parameters for mode 2.

    Returns
    -------
    calc:
        The updated calculator after the calculation.
    """
    if 'sfvalues' not in calc.results:
        raise RuntimeError('Please run mode 1 before mode 2.')

    # Update settings and run the job.
    calc.label = calc.label.split('mode1')[0] + 'mode2/mode2'

    # Bugfix for current ASE main: profile.prefix is not updated by label.
    calc.profile.prefix = calc.prefix

    calc.parameters.update(settings)
    calc.run(mode=2)

    if 'fitresults' in calc.results:
        fitresults = calc.results['fitresults']
        fitresults.table()

        plt.subplots(1, 1, figsize=(14, 6))
        fitresults.plot.rmse_e()
        plt.savefig(f'{calc.directory}/rmse_e.png', dpi=600)
        plt.close()

        if calc.parameters['use_short_forces'] is True:
            plt.subplots(1, 1, figsize=(14, 6))
            fitresults.plot.rmse_f()
            plt.savefig(f'{calc.directory}/rmse_f.png', dpi=600)
            plt.close()

        plt.subplots(1, 1, figsize=(14, 6))
        calc.results['weights'].plot.hist()
        plt.savefig(f'{calc.directory}/weights.png', dpi=600)
        plt.close()

    return calc


def run_mode3(
    calc: Runner,
    settings: Dict[str, Any]
) -> Runner:
    """Run Runner structure prediction.

    Parameters
    ----------
    calc:
        A Runner calculator that has already run mode 2.
    settings:
        A dictionary with user supplied parameters for mode 3.

    Returns
    -------
    calc:
        The updated calculator after the calculation.
    """
    if 'fitresults' not in calc.results:
        raise RuntimeError('Please run mode 2 before mode 3.')

    # Update settings and run the job.
    calc.label = calc.label.split('mode2')[0] + 'mode3/mode3'

    # Bugfix for current ASE main: profile.prefix is not updated by label.
    calc.profile.prefix = calc.prefix

    calc.parameters.update(settings)
    calc.run(mode=3)

    if 'energy' in calc.results:

        plt.subplots(1, 1, figsize=(14, 6))
        calc.plot.energy_hdnnp_vs_reference()
        plt.savefig(f'{calc.directory}/energy_hdnnp_vs_ref.png', dpi=600)
        plt.close()

        plt.subplots(1, 1, figsize=(14, 6))
        calc.plot.energy_deviation_vs_reference()
        plt.savefig(f'{calc.directory}/energy_dev_vs_ref.png', dpi=600)
        plt.close()

        plt.subplots(figsize=(14, 6))
        calc.plot.energy_distribution_reference()
        calc.plot.energy_distribution_hdnnp()
        plt.savefig(f'{calc.directory}/energy_distribution.png', dpi=600)
        plt.close()

        if calc.parameters['calculate_forces'] is True:
            plt.subplots(1, 1, figsize=(14, 6))
            calc.plot.force_hdnnp_vs_reference()
            plt.savefig(f'{calc.directory}/force_hdnnp_vs_ref.png', dpi=600)
            plt.close()

            plt.subplots(1, 1, figsize=(14, 6))
            calc.plot.force_deviation_vs_reference()
            plt.savefig(f'{calc.directory}/force_dev_vs_ref.png', dpi=600)
            plt.close()

            plt.subplots(figsize=(14, 6))
            calc.plot.totalforce_distribution_reference()
            calc.plot.totalforce_distribution_hdnnp()
            plt.savefig(
                f'{calc.directory}/totalforce_distribution.png',
                dpi=600
            )
            plt.close()

            plt.subplots(figsize=(14, 6))
            calc.plot.force_distribution_reference()
            calc.plot.force_distribution_hdnnp()
            plt.savefig(f'{calc.directory}/force_distribution.png', dpi=600)
            plt.close()

    return calc
