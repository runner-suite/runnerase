"""Implementation of ASE Atom mimic for temporary fast I/O."""

from typing import Optional, Iterator, Tuple, List

import numpy as np

from ase.atoms import Atoms
from ase.units import Hartree, Bohr

from runnerase.calculators.singlepoint import RunnerSinglePointCalculator


# A lot of structural information has to be stored.
# pylint: disable=too-many-instance-attributes
class TempAtoms:
    """Container for storing data about one atomic structure.

    This class is intended for fast I/O of structural data. It is required as
    the builtin ASE `Atoms` and `Atom` classes are a bottleneck for large data
    files. This is mostly because adding an `Atom` to an `Atoms` object over
    and over again comes with a lot of overhead because the attached arrays
    have to be checked and copied.
    In constrast, it is very efficient to store atomic positions, symbols, etc.
    in long lists and simply create one ASE `Atoms` object once all atoms have
    been collected. In summary, `TempAtoms` is simply a container to hold all
    these lists in one convenient place.
    """

    def __init__(self, atoms: Optional[Atoms] = None) -> None:
        """Initialize the object."""
        self.positions: List[List[float]] = []
        self.symbols: List[str] = []
        self.charges: List[float] = []
        self.magmoms: List[float] = []

        self.cell: List[List[float]] = []

        self.energy: float = np.NaN
        self.totalcharge: float = np.NaN
        self.forces: List[List[float]] = []

        # If given, read values from a given ASE Atoms object.
        if atoms is not None:
            self.from_aseatoms(atoms)

    def __iter__(
        self
    ) -> Iterator[Tuple[List[float], str, float, float, List[float]]]:
        """Iterate over tuples of information for each atom in storage."""
        for idx, xyz in enumerate(self.positions):
            element = self.symbols[idx]
            charge = self.charges[idx]
            atomenergy = self.magmoms[idx]
            force_xyz = self.forces[idx]

            yield (xyz, element, charge, atomenergy, force_xyz)

    @property
    def pbc(self) -> List[bool]:
        """Show the periodicity of the object along the Cartesian axes."""
        pbc = [False, False, False]
        for idx, vector in enumerate(self.cell):
            if len(vector) == 3:
                pbc[idx] = True

        return pbc

    def to_aseatoms(self) -> Atoms:
        """Convert the object to an ASE Atoms object."""
        atoms = Atoms(
            positions=self.positions,
            symbols=self.symbols,
            pbc=self.pbc
        )

        # Add the unit cell information.
        if any(self.pbc):
            atoms.set_cell(self.cell)

        atoms.set_initial_charges(self.charges)
        atoms.set_initial_magnetic_moments(self.magmoms)

        if np.isnan(self.totalcharge):
            self.totalcharge = 0.0

        calc = RunnerSinglePointCalculator(
            atoms,
            energy=self.energy,
            forces=self.forces,
            totalcharge=self.totalcharge
        )

        # The calculator has to be attached at the very end,
        # otherwise, it is overwritten by `set_cell()`, `set_pbc()`, ...
        atoms.calc = calc

        return atoms

    def from_aseatoms(self, atoms: Atoms) -> None:
        """Convert an ASE Atoms object to this class."""
        self.positions = list(atoms.positions)
        self.symbols = list(atoms.symbols)
        self.charges = list(atoms.get_initial_charges())
        self.magmoms = list(atoms.get_initial_magnetic_moments())

        if any(atoms.pbc):
            self.cell = atoms.cell

        if atoms.calc is not None:
            self.energy = atoms.get_potential_energy()
            self.forces = list(atoms.get_forces())
            if isinstance(atoms.calc, RunnerSinglePointCalculator):
                self.totalcharge = atoms.calc.get_property('totalcharge')
        else:
            self.energy = 0.0
            self.forces = [[0.0, 0.0, 0.0] for i in self.positions]
            self.totalcharge = np.sum(atoms.get_initial_charges())

    def convert(self, input_units: str, output_units: str) -> None:
        """Convert all data from `input_units` to `output_units`."""
        # Transform lists into numpy arrays for faster processing.

        if input_units == output_units:
            pass

        elif input_units == 'atomic' and output_units == 'si':
            self.positions = [[i * Bohr for i in xyz] for xyz in self.positions]
            self.cell = [[i * Bohr for i in xyz] for xyz in self.cell]
            self.energy *= Hartree
            self.forces = [[i * Hartree / Bohr for i in xyz]
                           for xyz in self.forces]

        elif input_units == 'si' and output_units == 'atomic':
            self.positions = [[i / Bohr for i in xyz] for xyz in self.positions]
            self.cell = [[i / Bohr for i in xyz] for xyz in self.cell]
            self.energy /= Hartree
            self.forces = [[i / Hartree * Bohr for i in xyz]
                           for xyz in self.forces]
