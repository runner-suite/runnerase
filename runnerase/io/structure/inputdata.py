"""Implementation of readers and writers for the RuNNer input.data file."""

from typing import Union, Iterator, List, TextIO

from ase.atoms import Atoms
from ase.utils import reader, writer
from ase.utils.plugins import ExternalIOFormat

from .tempatoms import TempAtoms

external_runner_format = ExternalIOFormat(
    desc='RuNNer input.data file',
    code='+F',
    module='runnerase.io.structure.inputdata',
    ext='data'
)


def _parse_chunk(chunk: str) -> TempAtoms:

    atoms = TempAtoms()

    for line in chunk.split('\n'):

        # Jump over blank lines and comments.
        if not line.strip() or line.strip().startswith('#'):
            continue

        # Split the line into the keyword and the arguments.
        keyword, arguments = line.split()[0], line.split()[1:]

        # Read one atom.
        if keyword == 'atom':
            xyz = [float(i) for i in arguments[:3]]
            symbol = arguments[3].lower().capitalize()

            charge = 0.0
            magmom = 0.0
            force_xyz = [0.0, 0.0, 0.0]
            if len(arguments) >= 9:
                charge = float(arguments[4])
                magmom = float(arguments[5])
                force_xyz = [float(i) for i in arguments[6:9]]

            atoms.positions.append(xyz)
            atoms.symbols.append(symbol)
            atoms.charges.append(charge)
            atoms.magmoms.append(magmom)
            atoms.forces.append(force_xyz)

        # Read one cell lattice vector.
        elif keyword == 'lattice':
            atoms.cell.append([float(i) for i in arguments[:3]])

        # Read the total energy of the structure.
        elif keyword == 'energy':
            atoms.energy = float(arguments[0])

        # Read the total charge of the structure.
        elif keyword == 'charge':
            atoms.totalcharge = float(arguments[0])

    return atoms


@reader
def read_runnerdata(
    infile: TextIO,
    index: Union[int, slice] = -1,
    input_units: str = 'atomic',
    output_units: str = 'si'
) -> Iterator[Atoms]:
    """Parse all structures within a RuNNer input.data file.

    input.data files contain all structural information needed to train a
    Behler-Parrinello-type neural network potential, e.g. Cart. coordinates,
    atomic forces, and energies. This function reads the file object `infile`
    and returns the slice of structures given by `index`. All structures will
    be converted to SI units by default.

    Parameters
    ----------
    infile:
        Python fileobj with the target input.data file.
    index:
        The slice of structures which should be returned. Returns only the last
        structure by default.
    input_units:
        The given input units. Can be 'si' or 'atomic'.
    output_units:
        The desired output units. Can be 'si' or 'atomic'.

    Returns
    -------
    images:
        All information about the structures within `index` of `infile`,
        including symbols, positions, atomic charges, and cell lattice. Every
        `Atoms` object has a `RunnerSinglePointCalculator` attached with
        additional information on the total energy, atomic forces, and total
        charge.

    """
    # First, split input.data into separate structure "chunks".
    read_infile = infile.read()
    # First chunk is discarded because it is the data before the
    # first "begin".
    chunks = read_infile.split('begin')[1:]

    # Second, only parse the chunks which the user asked for.
    for chunk in chunks[index]:

        atoms = _parse_chunk(chunk)

        # Convert all data to the specified output units.
        atoms.convert(input_units, output_units)

        # Append the structure to the list of all structures.
        aseatoms = atoms.to_aseatoms()

        yield aseatoms


@writer
def write_runnerdata(
    outfile: TextIO,
    images: List[Atoms],
    comment: str = '',
    fmt: str = '20.10f',
    input_units: str = 'si'
) -> None:
    """Write series of ASE Atoms to a RuNNer input.data file.

    For further details see the `read_runnerdata` routine.

    Parameters
    ----------
    outfile: TextIOWrapper
        Python fileobj with the target input.data file.
    images: array-like
        List of `Atoms` objects.
    comment: str
        A comment message to be added to each structure.
    fmt: str
        A format specifier for float values.
    input_units: str
        The given input units. Can be 'si' or 'atomic'.

    Raises
    ------
    ValueError: exception
        Raised if the comment line contains newline characters.
    """
    # Preprocess the comment.
    comment = comment.rstrip()
    if '\n' in comment:
        raise ValueError('Comment line cannot contain line breaks.')

    for atoms in images:

        # Transform into a TempAtoms object.
        tempatoms = TempAtoms(atoms)

        # Convert, if necessary.
        tempatoms.convert(input_units=input_units, output_units='atomic')

        # Begin writing this structure to file.
        outfile.write('begin\n')

        if comment != '':
            outfile.write(f'comment {comment}\n')

        # Write lattice vectors if the structure is marked as periodic.
        if any(tempatoms.pbc):
            for vector in tempatoms.cell:
                outfile.write(f'lattice {vector[0]:{fmt}} {vector[1]:{fmt}} '
                              f'{vector[2]:{fmt}}\n')

        for xyz, element, charge, atomenergy, force_xyz in tempatoms:
            outfile.write(f'atom {xyz[0]:{fmt}} {xyz[1]:{fmt}} {xyz[2]:{fmt}} '
                          f'{element:2s} {charge:{fmt}} {atomenergy:{fmt}} '
                          f'{force_xyz[0]:{fmt}} {force_xyz[1]:{fmt}} '
                          f'{force_xyz[2]:{fmt}}\n')

        # Write the energy and total charge, then end this structure.
        outfile.write(f'energy {tempatoms.energy:{fmt}}\n')

        tempatoms.totalcharge = 0.0

        outfile.write(f'charge {tempatoms.totalcharge:{fmt}}\n')
        outfile.write('end\n')
