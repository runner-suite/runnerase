#!/usr/bin/env python3
# encoding: utf-8
"""Routines to parse results RuNNer from all RuNNer modes."""

from typing import Dict, Tuple, TextIO

import os

import numpy as np

from ase.io import read
from ase.units import Hartree, Bohr
from ase.utils import reader
from ase.stress import full_3x3_to_voigt_6_stress

from runnerase.storageclasses import (RunnerSplitTrainTest, RunnerWeights,
                                      RunnerSymmetryFunctionValues,
                                      RunnerScaling,
                                      RunnerFitResults)


def check_successful(label: str) -> Tuple[bool, str]:
    """Read whether RuNNer terminated orderly.

    Parameters
    ----------
    label : str
        The ASE calculator label of the calculation. Typically this is the
        joined path of the `directory` and the .ase parameter file prefix.

    Returns
    -------
    success : bool
        True if the calculation finished successfully.
    message : str
        If success is False, this message contains the final three lines
        from the RuNNer stdout. Useful for debugging.
    """
    with open(f'{label}.out', 'rt', encoding='utf-8') as infile:
        lines = list(reversed(infile.readlines()))

    for line in lines[:5]:
        if 'Normal termination of RuNNer' in line:
            return True, ''

    message = 'RuNNer has not terminated successfully.\n'

    message += '=' * 17
    message += ' Final three lines captures from stdout are: '
    message += '=' * 17 + '\n'
    message += ''.join(lines[:3])
    message += '=' * 79

    return False, message


def read_results_mode1(label: str, directory: str) -> Dict[str, object]:
    """Read all results of RuNNer Mode 1.

    Parameters
    ----------
    label : str
        The ASE calculator label of the calculation. Typically this is the
        joined path of the `directory` and the .ase parameter file prefix.
    directory : str
        The path of the directory which holds the calculation files.

    Returns
    -------
    dict : RunnerResults
        A dictionary with two entries

        - sfvalues : RunnerSymmetryFunctionValues
            The symmetry function values.
        - splittraintest : RunnerSplitTrainTest
            The split between train and test set.
    """
    sfvalues_train = RunnerSymmetryFunctionValues(f'{directory}/function.data')
    sfvalues_test = RunnerSymmetryFunctionValues(f'{directory}/testing.data')
    splittraintest = RunnerSplitTrainTest(f'{label}.out')

    # Store only one symmetry function value container.
    sfvalues = sfvalues_train + sfvalues_test
    sfvalues.sort(splittraintest.train + splittraintest.test)

    return {'sfvalues': sfvalues, 'splittraintest': splittraintest}


def read_results_mode2(label: str, directory: str) -> Dict[str, object]:
    """Read all results of RuNNer Mode 2.

    Parameters
    ----------
    label : str
        The ASE calculator label of the calculation. Typically this is the
        joined path of the `directory` and the .ase parameter file prefix.
    directory : str
        The path of the directory which holds the calculation files.

    Returns
    -------
    dict : RunnerResults

        A dictionary with three entries

        - fitresults : RunnerFitResults
            Details about the fitting process.
        - weights : RunnerWeights
            The atomic neural network weights and bias values.
        - scaling : RunnerScaling
            The symmetry function scaling data.
    """
    # Store training results, weights, and symmetry function scaling data.
    fitresults = RunnerFitResults(f'{label}.out')

    # Mode 2 writes best weights to the optweights.XXX.out file.
    # runnerase determined the optimal epoch based on different criteria.
    epoch = fitresults.opt_rmse_epoch_runnerase
    prefix = f'{epoch:06d}.short'

    weights = RunnerWeights(path=directory, prefix=prefix, suffix='out')
    scaling = RunnerScaling(f'{directory}/scaling.data')

    return {'fitresults': fitresults, 'weights': weights, 'scaling': scaling}


def read_results_mode3(directory: str) -> Dict[str, object]:
    """Read all results of RuNNer Mode 3.

    Parameters
    ----------
    directory : str
        The path of the directory which holds the calculation files.

    Returns
    -------
    dict : RunnerResults

        A dictionary with two entries

        - energy : float or np.ndarray
            The total energy of all structures. In case of a single
            structure only one float value is returned.
        - forces : np.ndarray
            The atomic forces of all structures.
    """
    # Read predicted structures from the output.data file.
    # `read` automatically converts all properties to SI units.
    path = f'{directory}/output.data'
    pred_structures = read(path, index=':', format='runnerdata')

    energies = np.array([i.get_potential_energy() for i in pred_structures])
    forces = np.array([i.get_forces() for i in pred_structures], dtype='object')

    # Read stress from nnstress.out.
    path_stress = f'{directory}/nnstress.out'

    if os.path.exists(path_stress):
        stress = read_stress(path_stress)
    else:
        stress = np.zeros((forces.shape[0], 3, 3))

    # For just one structure, flatten the force arrays.
    # Cast dtype object back to float, because for one structure the array is
    # not ragged.
    if forces.shape[0] == 1:
        forces = forces[0, :, :].astype(float)
        stress = stress[0, :].astype(float)

    if energies.shape[0] == 1:
        return {'energy': float(energies[0]), 'forces': forces,
                'stress': stress}

    # Also read the weights and scaling for later access.
    weights = RunnerWeights(path=directory)
    scaling = RunnerScaling(f'{directory}/scaling.data')

    return {'energy': energies,
            'forces': forces,
            'stress': stress,
            'weights': weights,
            'scaling': scaling}


@reader
def read_stress(
    infile: TextIO,
    input_units: str = 'atomic',
    output_units: str = 'si'
) -> np.ndarray:
    """Read RuNNer nnstress.out file.

    Parameters
    ----------
    infile : TextIOWrapper
        The fileobj where the data will be read.
    input_units : str, _default_ 'atomic'
        The units within `images`. Can be 'si' or 'atomic'. All data will
        automatically be converted to `output_units`.
    output_units : str, _default_ 'si'
        The desired units in `data`. Can be 'si' or 'atomic'.

    Returns
    -------
    data : np.ndarray
        A 3x3xnum_atoms array holding the stress for each structure.
    """
    # The first row holds the column names.
    data: np.ndarray = np.loadtxt(infile, skiprows=1)

    # There are 3 lines of stress per structure.
    num_structures = int(data.shape[0] / 3)

    # Unit conversion.
    if input_units == 'atomic' and output_units == 'si':
        data[:, 1:] *= Hartree / Bohr**3

    elif input_units == 'si' and output_units == 'atomic':
        data[:, 1:] /= Hartree / Bohr**3

    data = data[:, 1:].reshape(num_structures, 3, 3)

    # Transform to Voigt notation.
    data = np.array([full_3x3_to_voigt_6_stress(i) for i in data])

    return data
