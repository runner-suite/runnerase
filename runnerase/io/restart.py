#!/usr/bin/env python3
# encoding: utf-8
"""Read and write routines required for restarting an existing calculation."""

from typing import Union, Tuple, List

import os

from ase.io import read
from ase.atoms import Atoms
from ase.calculators.calculator import Parameters

from runnerase.symmetry_functions.wrappers import SymmetryFunctionSet
from runnerase.io.inputnn import read_runnerconfig


def read_runnerase(
    label: str
) -> Tuple[Union[Atoms, List[Atoms], None], Parameters]:
    """Read structure and parameter options from a previous calculation.

    Parameters
    ----------
    label : str
        The ASE-internal calculation label, i.e. the prefix of the ASE
        parameters file.

    Returns
    -------
    atoms : ASE Atoms or List[Atoms]
        A list of all structures associated with the given calculation.
    parameters : Parameters
        A dictionary containing all RuNNer settings of the calculation.
    """
    # Extract the directory path.
    directory = '/'.join(label.split('/')[:-1])

    # Join the paths to all relevant files.
    inputdata_path = os.path.join(directory, 'input.data')
    aseparams_path = f'{label}.ase'
    inputnn_path = os.path.join(directory, 'input.nn')

    # Read structural information from the input.data file.
    if os.path.exists(inputdata_path):
        atoms = read(inputdata_path, index=':', format='runnerdata')
    else:
        atoms = None

    # Read RuNNer options first from the ASE parameter file and second from
    # the input.nn file.
    if os.path.exists(aseparams_path):
        parameters: Parameters = Parameters.read(aseparams_path)
        if 'symfunction_short' in parameters:
            parameters['symfunction_short'] = SymmetryFunctionSet(
                parameters['symfunction_short']
            )

    elif os.path.exists(inputnn_path):
        parameters = read_runnerconfig(inputnn_path)

    else:
        parameters = Parameters()

    return atoms, parameters
