#!/usr/bin/env python3
# encoding: utf-8
"""Routines for writing all input to a RuNNer calculation."""

from typing import Optional, Union, List

import os

from ase.atoms import Atoms
from ase.calculators.calculator import Parameters

from runnerase.storageclasses import (RunnerSplitTrainTest, RunnerWeights,
                                      RunnerSymmetryFunctionValues,
                                      RunnerScaling)

from runnerase.io.inputnn import write_runnerconfig
from runnerase.io.structure.inputdata import write_runnerdata
from runnerase.io.structure.traintest import (write_trainteststruct,
                                              write_traintestforces)


# RuNNer operates in several modi, all of which take different arguments.
# For clarity it is intentionally chosen to pass these explicitely, even though
# it increases the number of parameters.
# pylint: disable=too-many-arguments
def write_all_inputs(
    atoms: Union[Atoms, List[Atoms]],
    parameters: Parameters,
    label: str = 'runner',
    directory: str = '.',
    scaling: Optional[RunnerScaling] = None,
    weights: Optional[RunnerWeights] = None,
    splittraintest: Optional[RunnerSplitTrainTest] = None,
    sfvalues: Optional[RunnerSymmetryFunctionValues] = None
) -> None:
    """Write all necessary input files for performing a RuNNer calculation."""
    # All functions take a list of atoms objects as input.
    if not isinstance(atoms, list):
        atoms = [atoms]

    # Write all parameters to a .ase parameters file.
    parameters.write(f'{label}.ase')

    # Write the input.data file containing all structures.
    path = os.path.join(directory, 'input.data')
    write_runnerdata(path, atoms)

    # Write the input.nn file containing all parameters.
    path = os.path.join(directory, 'input.nn')
    write_runnerconfig(path, parameters)

    # Write scaling data.
    if scaling is not None:
        path = os.path.join(directory, 'scaling.data')
        scaling.write(path)

    # Write weights data.
    if weights is not None:
        weights.write(path=directory)

    if splittraintest is not None and sfvalues is not None:
        path = os.path.join(directory, 'function.data')
        sfvalues.write(path, splittraintest.train)

        path = os.path.join(directory, 'testing.data')
        sfvalues.write(path, splittraintest.test)

        path = os.path.join(directory, 'trainstruct.data')
        write_trainteststruct(path, atoms, splittraintest.train)

        path = os.path.join(directory, 'teststruct.data')
        write_trainteststruct(path, atoms, splittraintest.test)

        path = os.path.join(directory, 'trainforces.data')
        write_traintestforces(path, atoms, splittraintest.train)

        path = os.path.join(directory, 'testforces.data')
        write_traintestforces(path, atoms, splittraintest.test)
