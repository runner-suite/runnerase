"""Implementation of classes for symmetry function values.

Attributes
----------
RunnerStructureSymmetryFunctionValues: ElementStorageMixin
    Storage container for all symmetry function values of one single structure.
RunnerSymmetryFunctionValues:
    Storage container for many `RunnerStructureSymmetryFunctionValues` objects,
    i.e. for a collection of symmetry function values in one training dataset.
"""

from typing import Optional, Union, Tuple, List, TextIO, Dict

import numpy as np
from ase.data import atomic_numbers, chemical_symbols
from runnerase.utils.iodecorators import reader, writer

from runnerase.plot import (RunnerSymmetryFunctionValuesPlots,
                            RunnerStructureSymmetryFunctionValuesPlots)
from .mixins import ElementStorageMixin


class RunnerStructureSymmetryFunctionValues(ElementStorageMixin):
    """Storage container for the symmetry function values of one structure."""

    def __init__(
        self,
        energy_total: float = np.NaN,
        energy_short: float = np.NaN,
        energy_elec: float = np.NaN,
        charge: float = np.NaN,
    ) -> None:
        """Initialize the object.

        Parameters
        ----------
        energy_total:
            The total energy of the structure. Unit: Hartree.
        energy_short:
            The short-range part of the energy of the structure. Unit: Hartree.
        energy_elec:
            The electrostatic contribution to the energy of the structure.
            Unit: Hartree.
        charge:
            The total charge of the structure. Unit: electron charge.
        """
        # Initialize the base class. This will create the main data storage.
        super().__init__()

        # Save additional non-element-specific parameters.
        # Each parameter is stored in one large array for all structures.
        self.energy_total = energy_total
        self.energy_short = energy_short
        self.energy_elec = energy_elec
        self.charge = charge

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        return f'{self.__class__.__name__}(n_atoms={len(self)})'

    def by_atoms(self) -> List[Tuple[str, np.ndarray]]:
        """Expand dictionary of element symmetry functions into atom tuples."""
        data_tuples = []
        index = []

        for element, element_sfvalues in self.data.items():
            index += list(element_sfvalues[:, 0])
            sfvalues_list: List[np.ndarray] = list(element_sfvalues[:, 1:])

            for atom_sfvalues in sfvalues_list:
                data_tuples.append((element, atom_sfvalues))

        return [x for _, x in sorted(zip(index, data_tuples))]

    @property
    def plot(self) -> RunnerStructureSymmetryFunctionValuesPlots:
        """Create a plotting interface."""
        return RunnerStructureSymmetryFunctionValuesPlots(self.data)


class RunnerSymmetryFunctionValues:
    """Storage container for RuNNer symmetry function values.

    In RuNNer Mode 1, many-body symmetry functions (SFs) are calculated aiming
    to describe the chemical environment of every atom. As a result, every atom
    is characterized by a vector of SF values. These SF vectors always have the
    same size for each kind of element in the system.

    In the RuNNer Fortran code, this information is written to two files,
    'function.data' (for train set structures) and 'testing.data' (for test set
    structures). The files also contain additional information for each
    structure (all atomic units): the total energy, the short-range energy,
    the electrostatic energy, and the charge.
    """

    def __init__(self, infile: Optional[Union[str, TextIO]] = None) -> None:
        """Initialize the object.

        Parameters
        ----------
        infile:
            If given, data will be read from this fileobj upon initialization.
        """
        # Initialize the base class. This will create the main data storage.
        super().__init__()

        self.data: List[RunnerStructureSymmetryFunctionValues] = []

        # If given, read data from `infile`.
        if infile is not None:
            self.read(infile)

    def __len__(self) -> int:
        """Show the number of structures in storage."""
        return len(self.data)

    def __getitem__(self, index: int) -> RunnerStructureSymmetryFunctionValues:
        """Get the data for one structure at `index` in storage."""
        return self.data[index]

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        return f'{self.__class__.__name__}(n_structures={len(self)})'

    def __add__(
        self,
        blob: 'RunnerSymmetryFunctionValues'
    ) -> 'RunnerSymmetryFunctionValues':
        """Add a blob of symmetry function values to storage."""
        self.append(blob)
        return self

    def sort(self, index: List[int]) -> None:
        """Sort the structures in storage by `index`."""
        self.data = [x for _, x in sorted(zip(index, self.data))]

    def append(self, blob: 'RunnerSymmetryFunctionValues') -> None:
        """Append another blob of symmetry function values to storage."""
        for structure in blob.data:
            self.data.append(structure)

    @reader
    def read(self, infile: TextIO) -> None:
        """Read symmetry function values from `infile`."""
        allsfvalues: Dict[str, List[List[float]]] = {}
        idx_atom = 0
        for line in infile:
            spline = line.split()

            # Line of length 1 marks a new structure and holds the # of atoms.
            if len(spline) == 1:
                idx_atom = 0
                allsfvalues = {}
                structure = RunnerStructureSymmetryFunctionValues()

            # Line of length 4 marks the end of a structure.
            elif len(spline) == 4:
                structure.charge = float(spline[0])
                structure.energy_total = float(spline[1])
                structure.energy_short = float(spline[2])
                structure.energy_elec = float(spline[3])

                for element, data in allsfvalues.items():
                    structure.data[element] = np.array(data)

                self.data.append(structure)

            # All other lines hold symmetry function values.
            else:
                # Store the symmetry function values in the element dictionary.
                element = chemical_symbols[int(spline[0])]
                sfvalues = [float(i) for i in spline[1:]]

                if element not in allsfvalues:
                    allsfvalues[element] = []

                allsfvalues[element].append([float(idx_atom)] + sfvalues)
                idx_atom += 1

    @writer
    def write(
        self,
        outfile: TextIO,
        index: Union[int, slice, List[int]] = slice(0, None),
        fmt: str = '16.10f'
    ) -> None:
        """Write symmetry function scaling data."""
        # Retrieve the data.
        images: List[RunnerStructureSymmetryFunctionValues] = self.data

        # Filter the images which should be printed according to `index`.
        if isinstance(index, slice):
            images = images[index]
        elif isinstance(index, int):
            images = [images[index]]
        else:
            images = [images[i] for i in index]

        for image in images:
            # Start a structure by writing the number of atoms.
            outfile.write(f'{len(image):6}\n')

            # Write one line for each atom containing the atomic number followed
            # by the symmetry function values.
            for element, sfvalues in image.by_atoms():
                number = atomic_numbers[element]

                outfile.write(f'{number:3}')
                outfile.write(''.join(f'{i:{fmt}}' for i in sfvalues))
                outfile.write('\n')

            # End a structure by writing charge and energy information.
            outfile.write(f'{image.charge:{fmt}} {image.energy_total:{fmt}} '
                          + f'{image.energy_short:{fmt}} '
                          + f'{image.energy_elec:{fmt}}\n')

    @property
    def plot(self) -> RunnerSymmetryFunctionValuesPlots:
        """Create a plotting interface."""
        return RunnerSymmetryFunctionValuesPlots(self.data)
