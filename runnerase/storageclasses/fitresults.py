"""Implementation of classes for storing RuNNer fitting results from mode 2.

Attributes
----------
RunnerFitResults:
    Storage container for fit quality indicators in RuNNer Mode 2 stdout.
"""

from typing import Optional, Union, Tuple, List, Dict, TextIO

import re

import numpy as np

from runnerase.plot import RunnerFitResultsPlots
from runnerase.utils.iodecorators import reader


class RunnerFitResults:
    """Storage container for RuNNer training results.

    RuNNer Mode 2 generates a neural network potential in an iterative training
    process typical when working with neural networks. The information generated
    in course of this procedure enables the evaluation of the potential quality.
    This class stores typical quality markers to facilitate training process
    analysis:

    epochs:
        The number of epochs in the training process.
    rmse_energy:
        Root mean square error of the total energy. Possible keys are 'train',
        for the RMSE in the train set, and 'test', for the RMSE in the test set.
    rmse_force:
        Root mean square error of the atomic forces. See `rmse_energy`.
    rmse_charge:
        Root mean square error of the atomic charges. See `rmse_energy`.
    opt_rmse_epoch_runner:
        The number of the epoch were the best fit was obtained according to
        RuNNer.
    units:
        The units of the energy and force RMSE.
    """

    def __init__(self, infile: Optional[Union[str, TextIO]] = None) -> None:
        """Initialize the object.

        Parameters
        ----------
        infile:
            If given, data will be read from this fileobj upon initialization.
        """
        # Helper type hint definition for code brevity.
        RMSEDict = Dict[str, List[Optional[float]]]

        self.epochs: List[Optional[int]] = []
        self.rmse_energy: RMSEDict = {'train': [], 'test': []}
        self.rmse_forces: RMSEDict = {'train': [], 'test': []}
        self.rmse_charge: RMSEDict = {'train': [], 'test': []}
        self.opt_rmse_epoch_runner: Optional[int] = None
        self.opt_rmse_epoch_runnerase: Optional[int] = None
        self.units: Dict[str, str] = {'rmse_energy': '', 'rmse_force': ''}

        # If given, read data from `infile`.
        if infile is not None:
            self.read(infile)

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        num_epochs = len(self.epochs)
        return f'{self.__class__.__name__}(num_epochs={num_epochs}, ' \
               + f'best epoch={self.opt_rmse_epoch_runnerase})'

    @reader
    def read(self, infile: Union[str, TextIO]) -> None:
        """Read RuNNer Mode 2 results.

        Parameters
        ----------
        infile:
            Data will be read from this fileobj.
        """
        for line in infile:
            data = line.split()

            # Read the RMSEs of energies, forces and charges, and the
            # corresponding epochs.
            if line.strip().startswith('ENERGY'):
                epoch = int(data[1])
                if '*****' in line or len(data) < 5:
                    rmse_train, rmse_test = np.nan, np.nan
                else:
                    rmse_train, rmse_test = float(data[2]), float(data[3])

                self.epochs.append(epoch)
                self.rmse_energy['train'].append(rmse_train)
                self.rmse_energy['test'].append(rmse_test)

            elif line.strip().startswith('FORCES'):
                if '*****' in line or len(data) < 4:
                    rmse_train, rmse_test = np.nan, np.nan
                else:
                    rmse_train, rmse_test = float(data[2]), float(data[3])

                self.rmse_forces['train'].append(rmse_train)
                self.rmse_forces['test'].append(rmse_test)

            elif line.strip().startswith('CHARGE'):
                rmse_train, rmse_test = float(data[2]), float(data[3])
                self.rmse_charge['train'].append(rmse_train)
                self.rmse_charge['test'].append(rmse_test)

            # Read the fitting units, indicated by the heading 'RMSEs'.
            if 'RMSEs' in line:
                # Use regular expressions to find the units. All units
                # conveniently start with two letters ('Ha', or 'eV'), followed
                # by a slash and some more letters (e.g. 'Bohr', or 'atom').
                units: List[str] = re.findall(r'\w{2}/\w+', line)
                self.units['rmse_energy'] = units[0]
                self.units['rmse_force'] = units[1]

            # Read in the epoch where the best fit was obtained.
            if 'Best short range fit has been obtained in epoch' in line:
                self.opt_rmse_epoch_runner = int(data[-1])
                self.opt_rmse_epoch_runnerase = self.get_opt_rmse()[0]

            # Explicitely handle the special case that the fit did not yield any
            # improvement. This also means that no weights were written.
            if 'No improvement' in line:
                self.opt_rmse_epoch_runner = None
                self.opt_rmse_epoch_runnerase = None
                self.opt_rmse_epoch_runnerase = self.get_opt_rmse()[0]

    # Table returns many different pieces of information, therefore many
    # variables are needed.
    # pylint: disable=too-many-locals
    def table(self) -> None:
        """Print a tabular summary of the fitting results."""
        # Get the results of the fit.
        energy_train = self.rmse_energy['train']
        energy_test = self.rmse_energy['test']
        force_train = self.rmse_forces['train']
        force_test = self.rmse_forces['test']
        unit_energy = self.units['rmse_energy']
        unit_force = self.units['rmse_force']
        epochs = self.epochs

        if len(force_train) != len(energy_train):
            force_train = [np.nan for i in energy_train]
            force_test = [np.nan for i in energy_test]

        # Build the table header.
        colhead_energy = 'RMSE(E) / ' + unit_energy
        colhead_force = 'RMSE(F) / ' + unit_force
        header = f"Epoch | {colhead_energy:^18} | {colhead_force:^18} |"

        # Build the table subheader.
        subtraintest = f"{'Train':^8} | {'Test':^7}"
        subheader = f"{'':5s} | {subtraintest} | {subtraintest} |"

        # Print header, subheader, and their separator lines.
        print(header)
        print('=' * len(header))

        print(subheader)
        print('-' * len(subheader))

        # Print the results of each epoch.
        results = zip(epochs, energy_train, energy_test, force_train,
                      force_test)
        for epoch, e_train, e_test, f_train, f_test in results:

            print(f'{epoch:5d} '
                  + f'| {e_train:^9.4f}'
                  + f'| {e_test:^8.4f}'
                  + f'| {f_train:^9.4f}'
                  + f'| {f_test:^8.4f}|',
                  end='')

            if (
                epoch == self.opt_rmse_epoch_runner
                and epoch == self.opt_rmse_epoch_runnerase
            ):
                print(' <- Best Epoch')
            elif epoch == self.opt_rmse_epoch_runner:
                print(' <- Best Epoch according to RuNNer')
            elif epoch == self.opt_rmse_epoch_runnerase:
                print(' <- Best Epoch according to runnerase')
            else:
                print('')

    def get_opt_rmse(self, use_energy: bool = False) -> Tuple[int, float]:
        """Get epoch and value of the best testset energy/force RMSE.

        If `use_energy` is True, the energy test set RMSE is used, otherwise
        the force test set RMSE.
        """
        # If no forces were used for training or the user requested it, use the
        # energy RMSE.
        if len(self.rmse_forces['train']) == 0 or use_energy:
            best_epoch = self.opt_rmse_epoch_runner
            clean_rmse = [i for i in self.rmse_energy['test'] if i is not None]

            # If RuNNer did not detect an optimal epoch itself.
            if best_epoch is None:
                best_epoch = int(np.argmin(clean_rmse))

        else:
            clean_rmse = [i for i in self.rmse_forces['test'] if i is not None]
            best_epoch = int(np.argmin(clean_rmse))

        best_rmse = min(clean_rmse[best_epoch], 1e9)

        return (best_epoch, best_rmse)

    @property
    def plot(self) -> RunnerFitResultsPlots:
        """Create a plotting interface."""
        return RunnerFitResultsPlots(self.epochs, self.rmse_energy,
                                     self.rmse_forces, self.rmse_charge,
                                     self.opt_rmse_epoch_runner,
                                     self.opt_rmse_epoch_runnerase, self.units)
