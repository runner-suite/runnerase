"""Implementation of classes for storing RuNNer parameters.

Attributes
----------
RunnerWeights:
    Storage container for weights and bias values of atomic neural networks.
"""

from typing import Optional, List, TextIO

import os

import numpy as np
from ase.data import atomic_numbers, chemical_symbols

from runnerase.plot import RunnerWeightsPlots

from .mixins import ElementStorageMixin


class RunnerWeights(ElementStorageMixin):
    """Storage container for RuNNer neural network weights and bias values.

    Resources
    ---------
    For more information on the `weights.XXX.data` file format in RuNNer please
    visit the
    [docs](https://theochemgoettingen.gitlab.io/RuNNer/1.3/reference/files/).
    """

    # Weights can be read either from a single file (`infile` argument) or from
    # a set of files under the given `path`. In the latter case, `elements`,
    # `prefix`, and `suffix` need to be exposed to the user.
    # pylint: disable=too-many-arguments
    def __init__(
        self,
        infile: Optional[TextIO] = None,
        path: Optional[str] = None,
        elements: Optional[List[str]] = None,
        prefix: str = 'weights',
        suffix: str = 'data'
    ) -> None:
        """Initialize the object.

        Upon initialization, the user may specify to read weights either from
        a fileobj (`infile`) or from weight files residing under `path`.

        Parameters
        ----------
        infile:
            If given, data will be read from this fileobj upon initialization.
        path:
            If given, data will be read from all weight files under the given
            directory.
        elements:
            A selection of chemical symbols for which the weights under `path`
            will be read. Only active when `path` is given.
        prefix:
            The filename prefix of weight files under `path`. Only necessary
            when path is specified.
        suffix:
            The filename suffix of weight files under `path`. Only necessary
            when path is specified.
        """
        super().__init__()

        if infile is not None:
            self.read(infile)

        if path is not None:
            self.readall(path, elements, prefix, suffix)

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        # Get the number of weights for each element in storage.
        num_weights = []
        for element, data in self.data.items():
            num_weights.append(f'{element}: {data.shape[0]}')
        return f"{self.__class__.__name__}({', '.join(num_weights)})"

    def read(self, infile: TextIO) -> None:
        """Read the atomic neural network weights and biases for one element.

        Parameters
        ----------
        infile:
            Data will be read from this fileobj.
        """
        # Obtain the chemical symbol of the element. RuNNer names weights files
        # like `prefix`.{atomic_number}.`suffix`.
        atomic_number = int(infile.name.split('.')[1])
        element = chemical_symbols[atomic_number]

        # Store the weights as a np.ndarray.
        self.data[element] = np.genfromtxt(infile, usecols=0)  # type: ignore

    # For some reason pylint thinks that this function has the same signature as
    # ase.io.storageclasses.write_weights.
    # pylint: disable=R0801
    def readall(
        self,
        path: str = '.',
        elements: Optional[List[str]] = None,
        prefix: str = 'weights',
        suffix: str = 'data'
    ) -> None:
        """Read atomic neural network weights and bias values of many elements.

        Read all atomic neural network weight and bias files found under the
        specified path. The selection may be constrained by additional keywords.

        Parameters
        ----------
        path:
            Data will be read from all weight files found under the given
            path.
        elements:
            A selection of chemical symbols for which the weights under `path`
            will be read.
        prefix:
            The filename prefix of weight files under `path`.
        suffix:
            The filename suffix of weight files under `path`.
        """
        # If no elements were supplied, find all the element weights files at
        # the given path.
        if elements is None:
            elements = []
            for file in os.listdir(path):
                if file.startswith(prefix):
                    # Transform the atomic numbers into the chemical symbol.
                    element = chemical_symbols[int(file.split('.')[-2])]
                    elements.append(element)

        # Read in all desired element neural network weights.
        for element in elements:

            # Obtain the atomic number of the element and the path to the file.
            number = atomic_numbers[element]
            fullpath = os.path.join(path, f'{prefix}.{number:03d}.{suffix}')

            # Store the weights as a np.ndarray.
            self.data[element] = np.genfromtxt(fullpath,
                                               usecols=0)  # type: ignore

    def write(
        self,
        path: str = '.',
        elements: Optional[List[str]] = None,
        prefix: str = 'weights',
        suffix: str = 'data'
    ) -> None:
        """Write atomic neural network weights and biases for one element.

        Parameters
        ----------
        path:
            Data will be read from all weight files found under the given
            path.
        elements:
            A selection of chemical symbols for which the weights under `path`
            will be read.
        prefix:
            The filename prefix of weight files under `path`.
        suffix:
            The filename suffix of weight files under `path`.
        """
        for element, weights in self.data.items():
            # Skip over unspecified elements, if given.
            if elements is not None and element not in elements:
                continue

            # Write the data to file.
            number = atomic_numbers[element]
            element_path = os.path.join(path, f'{prefix}.{number:03d}.{suffix}')
            np.savetxt(element_path, weights, fmt='%.10f')

    @property
    def plot(self) -> RunnerWeightsPlots:
        """Create a plotting interface."""
        return RunnerWeightsPlots(self.data)
