"""Implementation of classes for storing RuNNer scaling data.

Attributes
----------
RunnerScaling:
    Storage container for RuNNer symmetry function scaling data.
"""

from typing import Optional, Union, List, Dict, TextIO

import numpy as np

from runnerase.utils.iodecorators import reader, writer
from runnerase.plot import (RunnerScalingPlots)
from .mixins import ElementStorageMixin


class RunnerScaling(ElementStorageMixin):
    """Storage container for RuNNer symmetry function scaling data.

    Resources
    ---------
    For more information on the `scaling.data` file format in RuNNer please
    visit the
    [docs](https://theochemgoettingen.gitlab.io/RuNNer/1.3/reference/files/).
    """

    def __init__(self, infile: Optional[Union[str, TextIO]] = None) -> None:
        """Initialize the object.

        Parameters
        ----------
        infile:
            If given, data will be read from this fileobj upon initialization.
        """
        # Initialize the base class. This creates the main self.data storage.
        super().__init__()

        # Store additional non-element-specific properties.
        self.target_min: float = np.NaN
        self.target_max: float = np.NaN

        # Read data from fileobj, if given.
        if infile is not None:
            self.read(infile)

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        return f'{self.__class__.__name__}(elements={self.elements}, ' \
               + f'min={self.target_min}, max={self.target_max})'

    @reader
    def read(self, infile: Union[str, TextIO]) -> None:
        """Read symmetry function scaling data.

        Parameters
        ----------
        infile:
            The fileobj containing the scaling data in RuNNer format.
        """
        scaling: Dict[str, List[List[float]]] = {}
        for line in infile:
            data = line.split()

            # Lines of length five hold scaling data for each symmetry function.
            if len(data) == 5:
                element_id = data[0]

                if element_id not in scaling:
                    scaling[element_id] = []

                scaling[element_id].append([float(i) for i in data[1:]])

            # The final line holds only the min. and max. of the target
            # property.
            elif len(data) == 2:
                self.target_min = float(data[0])
                self.target_max = float(data[1])

        # Transform data into numpy arrays.
        for element_id, scalingdata in scaling.items():
            npdata: np.ndarray = np.array(scalingdata)
            self.data[element_id] = npdata

    @writer
    def write(self, outfile: TextIO) -> None:
        """Write symmetry function scaling data.

        Parameters
        ----------
        outfile:
            The fileobj or path to which the scaling data will be written.
        """
        for element_id, data in self.data.items():
            # First, write the scaling data for each symmetry function.
            for line in data:
                outfile.write(f'{element_id:5s} {int(line[0]):5d}'
                              + f'{line[1]:18.9f} {line[2]:18.9f} '
                              + f'{line[3]:18.9f}\n')

        # The last line contains the minimum and maximum of the target property.
        outfile.write(f'{self.target_min:18.9f} {self.target_max:18.9f}\n')

    @property
    def plot(self) -> RunnerScalingPlots:
        """Create a plotting interface."""
        return RunnerScalingPlots(self.data)
