"""Classes for storing extrapolation warnings from LAMMPS simulations."""

from typing import List, TextIO, Dict, Any, Optional, Union

from collections import defaultdict

import numpy as np
from runnerase.utils.iodecorators import reader


class RunnerExtrapolationWarnings:
    """A class for storing extrapolation warnings from LAMMPS calculations.

    All extrapolation warnings are stored in `self.data` which is a List of
    dictionaries. Each dictionary contains the information from one
    extrapolation warning with the keys given by
    `self.default_extrapolation_mapping`.
    """

    def __init__(self, infile: Optional[Union[str, TextIO]] = None):
        """Initialize the class.

        Parameters
        ----------
        infile:
            The path to a LAMMPS simulation log. When `infile` is given, it
            is parsed and its contents are stored.
        """
        self.data: List[Dict[str, Any]] = []

        # Empty array for storing a large dictionary, where all extrapolation
        # information is joined into long arrays for each key.
        self._joined_dict: Optional[Dict[str, np.ndarray]] = None

        if infile is not None:
            self.read(infile)

    def __repr__(self):
        """Show a string representation of the object."""
        return f'RunnerExtrapolationWarnings(n={self.num_extrapolations})'

    @property
    def num_extrapolations(self):
        """Return the total number of extrapolation warnings."""
        return len(self.data)

    @property
    def default_extrapolation_map(self):
        """Provide a mapping for the extrapolation lines in lammps log file.

        Extrapolation warnings are printed in lines similar to the following
        one. For clarity, the index of each entry is shown above the line.
        0   1   2             3       4   5               6 7             8 \
        ### NNP EXTRAPOLATION WARNING ### STRUCTURE:      0 ATOM:         8 \

        9        10 11         12 13    14 15      16 \
        ELEMENT: Li SYMFUNC:   38 TYPE:  3 VALUE:  9.805E-01 \

        17    18        19    20
        MIN:  0.000E+00 MAX:  9.191E-01
        """
        return {
            'structure_idx': (6, int),
            'atom_idx': (8, int),
            'element': (10, str),
            'symfun_idx': (12, int),
            'symfun_type': (14, int),
            'symfun_val': (16, float),
            'symfun_min': (18, float),
            'symfun_max': (20, float)
        }

    @property
    def joined_dict(self) -> Dict[str, np.ndarray]:
        """Return a large dictionary containing all extrapolations.

        For each key in the extrapolation dictionaries, `joined_dict` will
        contain a 1D-numpy array containing the information of this key
        for all extrapolation warnings.
        """
        if self._joined_dict is None:
            self._joined_dict = self.join_dicts()

        return self._joined_dict

    @reader
    def read(self, infile: TextIO) -> None:
        """Parse extrapolation information from LAMMPS log file.

        Parameters
        ----------
        infile:
            The LAMMPS log file which will be parsed.
        """
        read_thermo = False
        current_timestep = 0
        for line in infile:
            if 'Step' in line:
                read_thermo = True

            if 'Loop time' in line:
                read_thermo = False

            if read_thermo and 'Step' not in line:
                spline = line.split()
                if len(spline) == 6:
                    current_timestep = int(line.split()[0])

            if 'EXTRA' in line:
                spline = line.split()

                extrapol = {}
                for key, val in self.default_extrapolation_map.items():
                    pos, convert = val
                    extrapol[key] = convert(spline[pos])

                extrapol['atom_idx'] -= 1  # Python indexes from 0, n2p2 at 1.
                extrapol['timestep'] = current_timestep

                self.data.append(extrapol)

    def join_dicts(
        self,
        dict_list: Optional[List[Dict[str, Any]]] = None
    ) -> Dict[str, np.ndarray]:
        """Join a list of dictionary into a dictionary of lists.

        As an example, this function transforms
        [{'a': 1, 'b': 2}, {'a': 3, 'b': 4}] into {'a': [1, 3], 'b': [2, 4]}.

        Parameters
        ----------
        dict_list:
            The list of dictionaries that will be joined. If not given,
             self.data is used.
        """
        results: Dict[str, List] = {i: []
                                    for i in self.default_extrapolation_map}
        results['timestep'] = []

        if dict_list is None:
            dict_list = self.data

        for extrapol in dict_list:
            for key in self.default_extrapolation_map:
                results[key].append(extrapol[key])

            results['timestep'].append(extrapol['timestep'])

        # Transform lists into numpy arrays.
        results_np = {}
        for key, arr in results.items():
            results_np[key] = np.array(arr)

        return results_np

    def get_steps(self) -> np.ndarray:
        """Create an array of all timesteps with extrapolation warnings."""
        steps = self.joined_dict['timestep']
        unique_steps = sorted(set(steps), key=list(steps).index)
        return np.array(unique_steps)

    def get_atom_indices(self, timestep: int) -> np.ndarray:
        """Create a unique array of all atoms extrapolating in a `timestep`."""
        extrapols_in_step = self.joined_dict['timestep'] == timestep
        atom_idx = self.joined_dict['atom_idx'][extrapols_in_step]
        return np.array(list(set(atom_idx)))

    def get_list(self, timestep: int) -> List[Dict[str, Any]]:
        """Create a list of all extrapolation warnings in one `timestep`."""
        extrapols = []
        for extrapol in self.data:
            if extrapol['timestep'] == timestep:
                extrapols.append(extrapol)

        return extrapols

    def get_list_grouped_atoms(
        self,
        timestep: int
    ) -> Dict[int, Dict[str, np.ndarray]]:
        """Get a list of extrapolations in `timestep`, grouped by atom_idx."""
        # {1: {'atom_idx': [], ...}, 2: {...}]

        extrapols = self.get_list(timestep)

        groups = defaultdict(list)
        for extrapol in extrapols:
            groups[extrapol['atom_idx']].append(extrapol)

        joined_groups = {}
        for key, group in groups.items():
            joined_groups[key] = self.join_dicts(group)

        return joined_groups


def get_severity(
    extrapolation: Union[Dict[str, np.ndarray], Dict[str, Any]]
) -> float:
    """Return the severity of a symmetry function extrapolation.

    The severity of an extrapolation is defined as the absolute amount that
    the symmetry function value during extrapolation is lower or higher
    than the minimum or maximum value of the symmetry function in the training
    dataset.
    For a list of extrapolations, the maximum severity is returned.
    """
    val = np.max(extrapolation['symfun_val'])
    minval = np.max(extrapolation['symfun_min'])
    maxval = np.max(extrapolation['symfun_max'])

    if val < minval:
        severity = minval - val
    if val > maxval:
        severity = val - maxval
    else:
        severity = 0.0

    return severity
