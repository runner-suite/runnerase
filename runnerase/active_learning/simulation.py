"""Implementation of a storage class for an active learning simulation."""

from typing import Optional, Any, Dict, List

import os
from copy import deepcopy
import pickle

import numpy as np
import matplotlib.pyplot as plt

from ase.io import read, write
from ase.atoms import Atoms
from ase.data import atomic_numbers

from runnerase.calculators.runner import Runner
from runnerase.storageclasses import RunnerExtrapolationWarnings, get_severity
from runnerase.analysis import RunnerAnalysis
from runnerase.plot import RunnerPlots
from runnerase.utils.sort import natsort
from runnerase.utils.atoms import extract_environment

Settings = Dict[str, Any]


# pylint: disable=too-many-instance-attributes
class RunnerActiveLearnSimulation:
    """A storage class for a single active learning simulation."""

    def __init__(
        self,
        parent_potential_idx: int,
        directory: Optional[str] = None,
        cutoff: Optional[np.ndarray] = None,
        potential_elements: Optional[List[str]] = None
    ) -> None:
        """Initialize the class.

        Parameters
        ----------
        parent_potential_idx:
            The index of the parent potential in the whole active learning
            cycle. Used for retrieving the recalculation that corresponds to
            the original MD trajectory.
        directory:
            Base path for storing all calculations related to this simulation.
            This includes the simulation dump and log, and a folder for all
            recalculations.
        cutoff: np.ndarray, optional
            The potential cutoff radius.
        potential_elements: List[str], optional
            A list of all elements in the potentials training dataset. Must be
            given when a directory is specified.
        """
        self.parent_potential_idx = parent_potential_idx
        self.directory: str = ''
        self.traj: List[Atoms] = []
        self.extrapolations: Optional[RunnerExtrapolationWarnings] = None
        self.recalculations: List[Runner] = []
        self.cutoff = cutoff

        # Storage containers for analysis.
        self._recalculation_analyzers: Optional[List[RunnerAnalysis]] = None
        self.statistics: Optional[Dict[str, Any]] = None

        if directory is not None:
            if potential_elements is None:
                raise RuntimeError('Cannot load simulation without element '
                                   'specification')
            self.read(directory, potential_elements)

    @property
    def recalculation_path(self):
        """Return the path to the folder of the recalculations."""
        return os.path.join(self.directory, 'recalculation')

    @property
    def statistics_filepath(self) -> str:
        """Return a path for storing statistics data as a pickle object."""
        return os.path.join(self.recalculation_path, 'statistics.pkl')

    @property
    def extrapolations_filepath(self) -> str:
        """Return a path for storing extrapolations as a pickle object."""
        return os.path.join(self.recalculation_path, 'extrapolations.pkl')

    @property
    def extrapolating_structures_filepath(self) -> str:
        """Return a path for storing extrapolating structures in XYZ format."""
        return os.path.join(
            self.recalculation_path,
            'extrapolating_structures.xyz'
        )

    @property
    def parent_recalculation(self):
        """Return the recalculation that uses the parent potential."""
        if len(self.recalculations) == 0:
            raise RuntimeError('Please load recalculations before '
                               'analyzing them.')

        return self.recalculations[self.parent_potential_idx]

    @property
    def recalculation_analyzers(self) -> List[RunnerAnalysis]:
        """Return the `RunnerAnalysis` object for each recalculation.

        Excludes the parent potential.
        """
        if len(self.recalculations) == 0:
            raise RuntimeError('Please load recalculations before '
                               'analyzing them.')

        if self._recalculation_analyzers is None:
            self._recalculation_analyzers = []
            for idx, calc in enumerate(self.recalculations):
                if idx == self.parent_potential_idx:
                    continue

                self._recalculation_analyzers.append(calc.analyze)

        return self._recalculation_analyzers

    def read(
        self,
        simulation_path: str,
        potential_elements: List[str]
    ) -> None:
        """Load simulation results.

        Loads the LAMMPS trajectory dump and reads extrapolation warnings
        from the logfile. After loading the trajectory, the atom types in the
        dump are replaced by the correct elements.

        Parameters
        ----------
        simulation_path:
            Path to the folder containing trajectory dump and log file.
        potential_elements:
            A list of all elements in by the potential dataset. Used for
            mapping the LAMMPS atom types back to the correct elements.
        """
        # Store the path.
        self.directory = simulation_path

        # Read the trajectory.
        trajfile = os.path.join(simulation_path, 'dump.lammpstrj')
        atoms = read(trajfile, index=':', format='lammps-dump-text')
        if isinstance(atoms, Atoms):
            atoms = [atoms]
        self.traj = atoms

        # LAMMPS dump files do not contain atom symbols, so we
        # have to correct the elements.
        for atoms in self.traj:

            correct_z = []
            for wrong_z in atoms.get_atomic_numbers():
                # pot.elements are sorted by atomic number.
                elem = potential_elements[wrong_z - 1]
                correct_z.append(atomic_numbers[elem])

            atoms.set_atomic_numbers(correct_z)

            # Read extrapolations.
            logfile = os.path.join(simulation_path, 'lammps.log')
            self.extrapolations = RunnerExtrapolationWarnings(logfile)

    def read_all_recalculations(self):
        """Load all the recalculated results of this trajectory.

        After the trajectory of the simulation has been recalculated by all
        potentials used in this active learning cycle, this routine will load
        the Runner calculator objects for all these recalculations.
        """
        calcs = []

        # Do nothing if no recalculations are found.
        if not os.path.exists(self.recalculation_path):
            return

        folders = os.listdir(self.recalculation_path)
        for recal_folder in sorted(folders, key=natsort):
            calc_path = os.path.join(self.recalculation_path, recal_folder)

            # Jump over possible files in the directory.
            if not os.path.isdir(calc_path):
                continue

            calc = Runner(
                restart=f'{calc_path}/mode3',
                command='',
                perform_check_successful=False
            )
            calcs.append(deepcopy(calc))

        self.recalculations = calcs

    def analyze_recalculations(self, plot: bool = True) -> None:
        """Analyze interpolations by checking stats between recalculations.

        Writes statistics reports and (if plot is True) also plots
        comparisons between the different recalculations.
        """
        plot0 = self.parent_recalculation.plot

        self.get_recalculation_statistics(plot0)
        self.write_statistics()

        if plot:
            self.plot_recalculations(plot0)

    def analyze_extrapolations(self) -> None:
        """Write extrapolation statistics and structures to files."""
        self.write_extrapolations()
        self.write_extrapolating_structures(
            whole_structure=True,
            margin=0.0
        )

    def get_recalculation_statistics(self, plot0: RunnerPlots) -> None:
        """Calculate mean / std dev for energies/forces over recalculations.

        `plot0` is the RunnerPlots (derives from RunnerAnalysis) object for
        the recalculation of the parent potential.
        """
        res_e_abs = plot0.energy_absolute_mean(self.recalculation_analyzers)
        res_e_error = plot0.energy_error_mean(self.recalculation_analyzers)
        res_f_abs = plot0.forces_absolute_mean(self.recalculation_analyzers)
        res_f_error = plot0.forces_error_mean(self.recalculation_analyzers)

        self.statistics = {
            'energy': {
                'absolute_values': res_e_abs,
                'error_to_reference': res_e_error
            },
            'forces': {
                'absolute_values': res_f_abs,
                'error_to_reference': res_f_error
            }
        }

    def plot_recalculations(self, plot0: RunnerPlots) -> None:
        """Generate comparison plots between the recalculations.

        `plot0` is the RunnerPlots (derives from RunnerAnalysis) object for
        the recalculation of the parent potential.
        """
        plot_functions = [
            plot0.comparison_energy_mean_vs_prediction,
            plot0.comparison_energy_error_mean_vs_prediction,
            plot0.comparison_forces_mean_vs_prediction,
            plot0.comparison_forces_error_mean_vs_prediction
        ]

        for func in plot_functions:
            plt.subplots(1, 1, figsize=(14, 6))
            func(self.recalculation_analyzers)
            plt.savefig(f'{self.recalculation_path}/{func.__name__}', dpi=300)
            plt.close()

    def write_statistics(self) -> None:
        """Write `self.statistics` to `self.statistics_filepath`."""
        if self.statistics is None:
            raise ValueError('No statistics information found. '
                             f'in {self.directory}.')

        with open(self.statistics_filepath, 'wb') as outfile:
            pickle.dump(self.statistics, outfile)

    def read_statistics(self) -> None:
        """Read `self.statistics` from `self.statistics_filepath`."""
        with open(self.statistics_filepath, 'rb') as infile:
            self.statistics = pickle.load(infile)

    def write_extrapolations(self) -> None:
        """Write `self.extrapolations` to `self.extrapolations_filepath`."""
        if self.extrapolations is None:
            raise ValueError('No extrapolation information found. '
                             f'in {self.directory}.')

        with open(self.extrapolations_filepath, 'wb') as outfile:
            pickle.dump(self.extrapolations, outfile)

    def read_extrapolations(self) -> None:
        """Read `self.extrapolations` from `self.extrapolations_filepath`."""
        with open(self.extrapolations_filepath, 'rb') as infile:
            self.extrapolations = pickle.load(infile)

    # pylint: disable=too-many-locals, too-many-branches
    # Reason: the user is supposed to be able to request different types
    # of extractions. This requires many branches and variables.
    def write_extrapolating_structures(
        self,
        whole_structure: bool = True,
        whole_trajectory: bool = False,
        filename: Optional[str] = None,
        margin: float = 0.0
    ) -> None:
        """Create an xyz file containing all extrapolating timesteps.

        This routine will write a trajectory in extendedXYZ containing the
        extrapolating structures in the simulation. Depending on the options,
        only the environment around the extrapolating atom (possibly with a
        margin) can be written. Whenever a file is written, it contains a
        "Severity" column, specifying the size of the extrapolation. This
        is very useful for plotting.

        Parameters
        ----------
        whole_structure:
            If True, whenever an atom extrapolates in a timestep, the whole
            structure is printed to the file. Otherwise, only the extrapolating
            atom and its atomic environment are written. In that case, one
            image is written for each atom and each timestep that is
            extrapolating (so there can be multiple images for one timestep
            because more than one atom are extrapolating).
        whole_trajectory:
            If True, write the entire trajectory to the file. It will include
            the severity column which allows users to watch the trajectory and
            see extrapolation warnings appearing and disappearing.
        filename: str, optional
            The name of the trajectory output file. If it is not set,
            `self.extrapolating_structures_filepath` is used.
        margin: float, _default_ 0.0
            When `whole_structure` is False, only the environment around the
            extrapolating atom is printed to a file. If margin > 0.0, a cubic
            box is chosen around the cutoff environment and also written. This
            enables fun things like equilibrating the atoms around the
            extrapolating atom while keeping the atomic environment still.
        """
        if self.extrapolations is None:
            raise ValueError('No extrapolation information found. '
                             f'in {self.directory}.')

        if whole_trajectory and not whole_structure:
            raise ValueError('If whole_trajectory is True, whole_structure '
                             'must be True, too.')

        if filename is None:
            filename = self.extrapolating_structures_filepath

        # Add array for storing severity to whole trajectory.
        for atoms in self.traj:
            atoms.arrays['Severity'] = np.zeros(len(atoms))

        extrapol_strucs = []
        steps = self.extrapolations.get_steps()

        if len(steps) == 0:
            return

        for step in steps:
            # Load trajectory and corresponding extrapolations.
            atoms = self.traj[step]

            if whole_structure:  # output the whole structure.

                extrapols = self.extrapolations.get_list(step)
                for extrapol in extrapols:
                    severity = get_severity(extrapol)
                    atom_idx = extrapol['atom_idx']
                    atoms.arrays['Severity'][atom_idx] = np.float64(severity)

                extrapol_strucs.append(atoms)

            else:  # output all extrapolating environments.
                atom_ext = self.extrapolations.get_list_grouped_atoms(step)

                for atom_idx, extrapol_group in atom_ext.items():
                    # Reset severities.
                    atoms.arrays['Severity'] = np.zeros(len(atoms))

                    # Get severity rating.
                    severity = get_severity(extrapol_group)
                    atoms.arrays['Severity'][atom_idx] = np.float64(severity)

                    if self.cutoff is None:
                        raise RuntimeError('A cutoff is needed to extract '
                                           'atomic environments.')

                    # Extract environment.
                    environment = extract_environment(
                        self.traj[step],
                        atom_idx,
                        self.cutoff,
                        severity,
                        margin=margin
                    )

                    extrapol_strucs.append(environment)

        # Print the whole trajectory, if requested.
        if whole_trajectory:
            atoms_to_write = self.traj
        else:
            atoms_to_write = extrapol_strucs

        # Write only those columns to extended XYZ format for
        # which the the first structure has values.
        columns_to_write = ['symbols', 'positions', 'Severity']
        maybe_arrays = ['initial_charges', 'initial_magmoms']
        for arrname in maybe_arrays:
            if arrname in extrapol_strucs[0].arrays:
                columns_to_write.append(arrname)

        write(filename, atoms_to_write, columns=columns_to_write)
