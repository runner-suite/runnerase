"""Implementation of a storage class for an active learning potential."""

from typing import Optional, Any, Dict, List

import os

import numpy as np

from runnerase.calculators.runner import Runner
from runnerase.utils.atoms import get_elements
from runnerase.utils.sort import natsort

from .simulation import RunnerActiveLearnSimulation

Settings = Dict[str, Any]


class RunnerActiveLearnPotential:
    """A storageclass for a RuNNer potential to be used in active learning."""

    def __init__(
        self,
        directory: Optional[str] = None,
        simulations: Optional[List["RunnerActiveLearnSimulation"]] = None,
        calc: Optional[Runner] = None,
        command: Optional[str] = None
    ) -> None:
        """Initialize the class.

        Parameters
        ----------
        directory:
            Base path for storing all calculations related to this potential.
            This includes the training data (RuNNer modes 1, 2 and 3) and a
            folder for all simulations.
            The name of the folder itself should always end with a continuous
            index to differentiate the potential from others in the training
            cycle, separated from the rest of the name by an underscore
            (e.g. "pot_0"). When a directory is given during initialization,
            the HDNNP in it will be loaded as a `Runner` object.
        simulations:
            A list of all LAMMPS simulations that were performed with this
            potential.
        calc:
            Runner calculator that is used for training this potential
            (stores all training results).
        command:
            The command for the Runner calculator.
        """
        self.directory = directory
        self.simulations = simulations or []
        self.calc = calc
        self.runner_cmd = command

        if directory is not None:
            self.read(directory)

    @property
    def simulation_path(self):
        """Return the path to the simulation folder."""
        return os.path.join(self.directory, 'simulations')

    @property
    def index(self):
        """Return the index of this potential in the training cycle."""
        return int(self.directory.split('_')[-1])

    def read(
        self,
        potential_path: str
    ) -> None:
        """Load the HDNNP stored under `potential_path`.

        Sets `self.directory` to `potential_path`.
        """
        self.directory = potential_path
        calc_path = f'{potential_path}/mode3/mode3'
        self.calc = Runner(
            restart=calc_path,
            label=calc_path,
            command=self.runner_cmd
        )

    def read_all_simulations(self) -> None:
        """Parse all simulations under `self.simulation_path`."""
        if self.calc is None:
            raise RuntimeError('Please read a potential before reading '
                               'simulations.')

        if self.calc.dataset is None:
            raise RuntimeError('Calculator does not contain a dataset.')

        # Obtain the cutoff radius for this potential.
        cutoffs = self.calc.parameters['symfunction_short'].cutoffs
        cutoff = cutoffs[0]
        if len(cutoffs) > 1:
            raise RuntimeError(
                'LAMMPS simulation is only supported with a '
                + 'uniform cutoff radius.'
            )

        elements = get_elements(self.calc.dataset, sort_by_appearance=True)
        folders = os.listdir(self.simulation_path)
        for sim_folder in sorted(folders, key=natsort):

            sim = RunnerActiveLearnSimulation(
                parent_potential_idx=self.index,
                cutoff=np.array([cutoff] * 3)
            )
            sim.read(os.path.join(self.simulation_path, sim_folder), elements)

            self.simulations.append(sim)
