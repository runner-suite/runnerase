# Changelog
All notable changes to this project will be documented in this file.

The format of this file is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2023-11-06

Updated documentation about Pypi problems.

## [1.0.1] - 2023-11-06

Small documentation fixes.

## [1.0.0] - 2023-11-01
The first major release version of runnerase outside of experimental mode
bring a lot of new features and bugfixes. It integrates well with the
currently unreleased (as of 2023-11-01) main branch of ASE (v3.23.0b1).
This release also marks major enhancements to the online documentation
with many helpful examples.

The addition of a progress bar introduces a new optional dependency
library tqdm. Its use is enabled by default and greatly encouraged.

### TODO
- Add back read/write routines from before and deprecate them.
  - Analysis:
    - Report structures with highest energy / force dev as atoms object and list of errors
        - only the extrapolating environment (possibly with a margin around it)
- easier plot styling

### Fixed
- Various small bugfixes in the parsing of forces (Mode 3) and fit results
  (Mode 2).

### Improved
- **I/O:**
  - The read/write of input.data now register with the ASE `read` and
    `write` routines. This feature requires ASE >= v3.23.0b1.
    The internal `read` and `write` routines of runnerase are now
    deprecated and their use is discouraged.
  - The `read` routine now respects the `index` selection already during
    parsing. This means that the routine is considerably faster when
    only parsing a subset of the data.
  - The efficiency of reading, writing and storing symmetry functions
    was greatly improved. While it used to be a bottleneck when working
    with many (>=5) elements, the time spent on this is now neglectable.

- **`Runner` calculator:**
  - The calculator objects now has a nice string representation when
    calling `print(calc)`.

### Added New
- **`Runner` calculator:**
  - `sample` routine for selecting random minibatches from `self.dataset`.
  - `filter` routine for applying an arbitrary filter function to
    `self.dataset`.
  - The lattice stress is now parsed a result (`calc.results['stress']`)
    when it is calculated.
  - When RuNNer results are read, the calculator will now check whether
    the calculation was successful. If it was not, it will stop with a
    `RuntimeError` and print the last three lines of the captures stdout
    for helpful debugging.
  - The `run` routine will now track the calculation status and print
    continuous information in the form of a progress bar.

- The nifty `submit_slurm` function allows to submit calculations
  to a local or remote queueing system. It can wrap any function call.

- Added two **workflows**:
  - `train` workflow will run a full training job (Modes 1, 2 and 3),
    perform some basic analysis and plotting. It can be submitted to the
    queue and fully customized with dictionaries of settings.
  - `run_nvt_simulation` directly connects runnerase to LAMMPS for the
    simulation of large-scale systems. It can easily serve as a blueprint
    for the submission of custom and arbitrarily complex LAMMPS workflows.

- **Active learning module:**
  - The `RunnerActiveLearn`, `RunnerActiveLearnPotential` and
    `RunnerActiveLearnSimulation` enable extensible submission and 
    analysis of active learning cycles, including:
    - Training a certain number of HDNNPs on the same dataset (parallelized)
      via the queueing system.
    - Run parallel simulations based on the different potentials.
    - Recalculate all trajectories with all potentials.
    - Analyze extrapolations and interpolations:
      - Compare predictions between HDNNPs.
      - Find and extract extrapolating environments.
      - Get recommendations for structures which are still needed in the
        dataset.

- A new **`RunnerAnalysis` class**  was introduced which mostly helps
  with analysing the dataset attached to a calculator:
  - A `mask` property allows choosing only a subset of the dataset in all
    analysis that is performed.
  - `get_rmse_mae()` routine for the calculation of root mean square
    error and mean average error.
  - `binary_convex_hull` will retrieve the lowest energy configurations
    grouped by composition in `self.dataset`.
  - Averaging and comparison of multiple HDNNPs.
  - ... and many more (check out the documentation).

- **`RunnerPlots` class:**
  - The `RunnerPlots` class is now based on `RunnerAnalysis`.
  - Comparison plots between multiple neural networks have been added.

- `generate_symmetry_functions` now allows a new algorithm for radial
  symmetry functions: shifted.

- **Visualization:**
  - Connection to OVITO allows automatic generation of pipelines. This
    enables further analysis with all of the features implemented in
    OVITO or even makes it possible to directly open a dataset in OVITO
    PRO.
  - As mentioned above, the integration of the read/write routines for
    input.data makes it possible to call `ase gui input.data` to
    visualize datasets with one short call from the command line.

- **Improved and extended documentation:**
  - Documentation link on pypi has been corrected.
  - Documentation of installation including ovitos and LAMMPS modules.
  - Better API documentation.
  - Tutorials and first steps were expanded considerably.

## [0.3.1] - 2023-04-26
### Fixed
- RunnerCalculator now stores the forces of a single structure as dtype float.
  For multiple structures, numpy requires dtype object as the array is ragged
  (3 x num_atoms_of_each_structure x num_structures).

## [0.3.1] - 2023-01-17
### Fixed
- RunnerCalculator can now read forces again.
- All plot interfaces have been updated to work with ragged arrays of forces.
- When writing input.data, the totalcharge is now always calculated from the
  sum of atomic charges, unless specified explicitely in  a
  `RunnerSinglePointCalculator` object that is attached to the `Atoms` object.
- Various mypy version bumps.

## [0.3.0] - 2022-06-17
### Added
- `Runner` calculator now has a plotting interface for visualization of
  information about the dataset, energies, forces, comparison between HDNNP and
  reference energy, forces, etc.
- `RunnerWeights` now has a plotting interface.
- `SymmetryFunctionSet` and `SymmetryFunction` now both have a `todict` routine.
  This enables the calculator to be written to ASE trajectory files.

### Fixed
- `RunnerScalingPlots` and `RunnerSymmetryFunctionValuesPlots` now correctly
  separate boxes and bars of different elements in their plots.
- Corrected the unit in the radial symmetry function legend.
- Improved the handling of the calculator label and directory.
- `read` function now correctly passes on a filename to ASE.
- `restart` can now also be triggered from a completely different directory.

## [0.2.2] - 2022-06-01
### Added
- `SymmetryFunctionSetPlots` now also works for multiple elements.
- `SymmetryFunctionSetPlots` supports showing the cutoff function for radial
  symmetry functions.
- `SymmetryFunctionSetPlots` now have a switch for enabling the legend.

### Fixed
- `RunnerFitresults` now stores the correct optimal epoch. Before, it was off
  by 1.

## [0.2.1] - 2022-05-31
### Fixed
- Add `MANIFEST.in` so that `.mplstyle` files are installed together with the
  package.

## [0.2.0] - 2022-05-30
### Added
- Add a general architecture for enabling plots of different RuNNer input and
  output properties.
- Plotting interfaces are now available for `RunnerFitResults`, `RunnerScaling`,
  `RunnerSymmetryFunctionValues`, `RunnerStructureSymmetryFunctionValues`,
  `SymmetryFunctionSet`, and `RunnerSplitTrainTest`.
- Add a `reset` function to `SymmetryFunctionSet` which clears all symmetry
  functions from storage.
- Add a `table` property to `RunnerFitResults` which prints the energy and
  force RMSEs including the optimal epoch.

### Fixed
- Minimum distance calculations in `calc_min_distances` is now skipped when
  there is only one atom of the element available in a structure.
- Minimum distances are now converted to and stored in Bohr instead of Angstrom.
- `SymmetryFunctionSet`.`cutoffs` now prints the collective cutoff of all
  symmetry functions in `storage` instead of only the ones part of
  `symmetryfunctions`.
- Some minor maintenance.

## [0.1.1] - 2022-04-28
### Added
- `SymmetryFunction` class now also recognized `sftype` 23 - 27, so the
  interface works with mHDNNPs.

### Fixed
- Make `read` function work correctly with ASEs `read` function.
- Enable empty initialization of `RunnerFitResults` and `RunnerSplitTrainTest`.
  The two containers up to now required the keyword argument `infile` for
  initialization. This has now become optional.
- Clear `RunnerSplitTrainTest` storage when reading information from a new
  file.
- Fix faulty unit conversion of atomic forces when reading input.data files.
- Fix wrong argument formatting when writing input.nn files.

## [0.1.0] - 2022-04-28
### Added
- The first standalone version of this project.

[Unreleased]: https://gitlab.com/runner-suite/runnerase/-/compare/0.1.0...main
[0.1.0]: https://gitlab.com/runner-suite/runnerase/-/tags/0.1.0
[0.1.1]: https://gitlab.com/runner-suite/runnerase/-/tags/0.1.1
[0.2.0]: https://gitlab.com/runner-suite/runnerase/-/tags/0.2.0