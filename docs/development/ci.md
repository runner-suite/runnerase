Whenever a user pushes a commit to the
Gitlab repository of the project, an automated
[Continuous Integration pipeline](https://docs.gitlab.com/ee/ci/introduction/)
(CI pipeline) is triggered. CI pipelines serve the purpose of checking and
testing code for correctness, formatting, and functionality before a new commit
is permanently added to the project's source code.

The CI pipeline is configured in the
[.gitlab-ci.yml](https://gitlab.com/runner-suite/runnerase/-/blob/main/.gitlab-ci.yml)
Please take a look at the comments in this file for further information on the
setup of the CI pipeline.

## Documentation
As one part of the deploy stage of the CI pipeline, the documentation is
generated. This happens every time a new Gitlab tag is added to a commit. Most
often, this will be whenever a new version of the package is published.

`runnerase` uses `mike` for versioning, which means that the new version of the
documentation is automatically build based on the `main` branch and then pushed,
during the CI pipeline execution, to the `manual` branch. **In other words: do
NEVER edit the `manual` branch by hand!**.
All pushes to the manual branch are automatically handled by our `runner-bot`
user. His personal Gitlab token (necessary CI variable `PROJECT_BOT_TOKEN`) can
be modified in the Gitlab settings of the project under 'CI/CD -> Variables'. 
