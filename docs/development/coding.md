This document discusses working on the `runnerase` code base. If you would like
to contribute to the documentation (which is much appreciated) please visit
[Improving the Documentation](documentation.md). For details on the setup of the CI
pipeline, take a look at [Extending the CI Pipeline](ci.md).

## Code of Conduct
All contributors are expected to follow the
[PSF Code of Conduct](https://www.python.org/psf/conduct/).

## Development Process
1. As a first-time contributer:
    * The code of runnerase is managed on [gitlab.com](https://gitlab.com/) as
      part of the RuNNer suite.
    * **Please read this document before contributing.**
    * Create your own copy of the code repository by forking
      [the project](https://gitlab.com/runner-suite/runnerase.git).
    * Clone the project to your local computer and change directory:

        ```sh
        git clone https://gitlab.com/your-username/runnerase.git
        cd runnerase
        ```

    * Add the upstream repository:

        ```sh
        git remote add upstream https://gitlab.com/runner-suite/runnerase.git
        ```

        Now, `git remote -v` will show two remote repositories named:

            * `upstream`, which refers to the official `runnerase` repository,
            * `origin`, which refers to your personal fork.

    * Install the package locally.

        ```sh
        pip install -e .[test,deploy]
        ```

2. Develop your contribution:

    * Pull the latest changes from upstream:

        ```sh
        git checkout main
        git pull upstream main
        ```

    * Create a branch for the feature you want to work on.

        ```sh
        git checkout -b feat-symmetryfunction-newalgorithm
        ```

        Since the branch name will appear in the merge message, use a sensible
        and descriptive name. Please prefix your branch name with a classifier for
        the contribution. Here is a selection:

          * `bugfix-` for bug fixes,
          * `ci-` for changes to the CI pipeline,
          * `doc-`, for work on the documentation,
          * `feat-` or `enh`, for new features or enhancements.

    * Commit locally as you progress (git add and git commit). Commit often and
      in small bites, this makes code review much easier. Use a proper
      commit message consisting of a short heading (<72 characters), and if
      necessary a descriptive and verbose commit message body, separated by
      a blank line.

    * For any contribution, write tests that fail before your change and
      pass afterward. Run all the tests locally to make sure that all existing
      tests will also pass.

    * Document any new procedures, modules, classes, ... with proper docstrings.
      Keep to the NumPy docstring standard.

    * Use pylint, flake8, mypy, and pydocstyle to format your code.

3. Submit your contribution:

    * Push your changes back to your fork:

        ```
        git push origin [branchname]
        ```

    * On Gitlab, create a pull request with a descriptive title and clear
      message.

    * Wait for code review.

## Stylistic Guidelines
`runnerase` follows PEP 8 style guidelines. Please set up your code editor to
follow them by installing linters. All contributions to the project should be
checked with pylint, flake8, mypy (for static type hints), and pydocstyle
(for proper docstring formatting).

### Docstring Formatting
All modules, functions, classes, ... should have a docstring in the NumPy
docstring format. This is necessary because the API documentation will be
automatically generated based on the information in the docstrings. Feel free
to check out other parts of the code base for examples. 

### Type Hints
The code of `runnerase` uses static type hints as specified in
[PEP484](https://peps.python.org/pep-0484/).

## Test Coverage

Pull requests (PRs) that modify code should either have new tests, or modify
existing tests to fail before the PR and pass afterwards.
You should run the tests locally before pushing a PR.

Running `runnerase`'s test suite locally requires some additional packages.
They can be easily installed with

```sh
pip install -e .[test,deploy]
```

Tests for a module should ideally cover all code in that module, i.e.,
statement coverage should be at 100% (even though this is rarely achieved).
