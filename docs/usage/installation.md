## Production Environment
### Package installation

runnerase is published as a
[Python package](https://pypi.org/project/runnerase/) on Pypi
and can easily be installed with `pip`. It is recommended to create
a fresh new virtual environment or conda environment first:

```sh
# using pyenv, with Python version 3.11.
pyenv virtualenv 3.11 runnerase
pyenv activate runnerase

# using virtualenv
python -m venv venv
source venv/bin/activate

# using conda
conda create --name runnerase
conda active runnerase
```

Afterwards, you can install the package like this:

```sh
pip install runnerase[progressbar]
```

!!! warning
    At the moment, v1.0.* of runnerase must be installed from the Gitlab
    package repository. This is because the dependency ASE has not released
    v3.23 yet, which introduces major breaking changes in the way calculators
    are handeled. We are on this and will release a new package as things
    happen.

    For now, do
    `pip install runnerase[progressbar] --index-url https://gitlab.com/api/v4/projects/35567400/packages/pypi/simple`

This will automatically install compatible versions of the required
dependencies [ASE](https://wiki.fysik.dtu.dk/ase/),
[numpy](https://numpy.org/) and [scipy](https://scipy.org/). Moreover,
the value in square brackets marks the installation of the optional dependency
group "progressbar". This allows users to track the status of their RuNNer
calculations using [tqdm](https://github.com/tqdm/tqdm) progress bars.

Test that your installation was successful by opening an interactive Python
session and loading the Runner calculator. If this runs without error,
runnerase has been successfully installed.

```sh
python
>>> from runnerase.calculators.runner import Runner
>>>
```

### Installation with OVITO

In order to make use of the functions included in the `runnerase.visualize.ovito`
module, install the Python ovito module with pip:

```sh
pip install ovito
```

If you want to use runnerase through the `ovitos` Python interpreter
you have to install runnerase directlywith ovitos:

```sh
ovitos -m pip install --user runnerase[progressbar]
```

### Installation for using runnerase with LAMMPSlib

runnerase can automatically interface with LAMMPS to run large-scale
simulations. This requires the compilation of the LAMMPS Python interface.
Detailed information about the installation of this package can be found
[in the LAMMPS Programmer Guide](https://docs.lammps.org/Python_head.html).
Moreover, the documentation also contains much information on the
[general compilation](https://docs.lammps.org/Install.html) of the programm.

#### Conda
LAMMPS provides [a conda package](https://anaconda.org/conda-forge/lammps)
that can be installed like this

```sh
 conda install -c conda-forge lammps
```

It includes all necessary libraries.

#### Manual compilation

Here, we present one way to install the LAMMPS Python package using the Intel
oneAPI. Apart from ML-HDNNP, we install a list of other packages useful for
running simulations with a HDNN potential.

First, obtain a current copy of the source code:

```sh
git clone -b release https://github.com/lammps/lammps.git lammps-runnerase
```

Next, prepare your environment by accessing your runnerase virtual environment
and loading all required modules (for example Intel Compilers, MPI, ...)

```sh
pyenv active runnerase
module load intel/mkl/ intel/ifort/ mpi/openmpi-x86_64
```

Now, we build a serial version of LAMMPS as a shared library with Python
support using cmake:

```sh
# Create a build directory.
mkdir build
cd build

# Set the installation location inside the virtual environment.
echo $VIRTUAL_ENV
export CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV

# Configure using cmake.
cmake ../cmake \
    -D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV \
    -D CMAKE_C_COMPILER=icc \
    -D CMAKE_CXX_COMPILER=icpc \
    -D CMAKE_Fortran_COMPILER=ifort \
    -D TBB_ROOT_DIR=/opt/intel/oneapi/tbb \
    -D BUILD_MPI=no \
    -D BUILD_OMP=yes \
    -D BUILD_SHARED_LIBS=yes \
    -D FFT=FFTW3 \
    -D PKG_COMPRESS=yes \
    -D PKG_ELECTRODE=yes \
    -D PKG_KSPACE=yes \
    -D PKG_INTEL=yes \
    -D PKG_MANYBODY=yes \
    -D PKG_MISC=yes \
    -D PKG_OPENMP=yes \
    -D PKG_OPT=yes \
    -D PKG_REAXFF=yes \
    -D PKG_RIGID=yes \
    -D PKG_ML-HDNNP=yes \
    -D PKG_PYTHON=yes \
    -D PYTHON_EXECUTABLE=$VIRTUAL_ENV/bin/python3 \
    -D LAMMPS_EXCEPTIONS=yes

# Build on 8 cores.
cmake --build . -j8

# Install the files to the virtual environment.
make install-python
```

After these steps have succeeded, we still need to add the path to the LAMMPS
shared library to the LD_LIBRARY_PATH. This needs to be done every time you activate
the virtual environment and plan to call LAMMPS:

```sh
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$VIRTUAL_ENV/lib/python3.11/site-packages/lammps"
```

Finally, we can test whether the lammps object can be called. If the final
command does not give your an error or segfault, you are probably good to go.

```sh
python
>>> import lammps
>>> lmp = lammps.lammps()
```

## Development Environment

For development purposes, you may want to obtain the source code by cloning
the Git repository:

```sh
git clone https://gitlab.com/runner-suite/runnerase
```

You can then install the package in pip's 'editable' mode which will
automatically reflect all changes you make to the source code in the new
project subfolder:

```sh
pip install -e .[progressbar,test,deploy]
```

This command also includes the optional dependencies required for running the
test suite and building the documentation locally.
